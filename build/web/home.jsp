<%-- 
    Document   : home
    Created on : Oct 1, 2022, 9:47:14 PM
    Author     : User
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

    <head>
        <!-- Basic -->
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <!-- Site Metas -->
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <link rel="shortcut icon" href="images/LogoShoes.png" type="">

        <title> KSHOES</title>

        <!-- bootstrap core css -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />

        <!--owl slider stylesheet -->
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />
        <!-- nice select  -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/css/nice-select.min.css" integrity="sha512-CruCP+TD3yXzlvvijET8wV5WxxEh5H8P4cmz0RFbKK6FlZ2sYl3AEsKlLPHbniXKSrDdFewhbmBK5skbdsASbQ==" crossorigin="anonymous" />
        <!-- font awesome style -->
        <link href="css/font-awesome.min.css" rel="stylesheet" />

        <!-- Custom styles for this template -->
        <link href="css/style.css" rel="stylesheet" />
        <!-- responsive style -->
        <link href="css/responsive.css" rel="stylesheet" />
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Bootstrap CRUD Data Table for Database with Modal Form</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <style>
            h6{
                color:white;
                font-size: 12px;
            }
            .old{
                text-decoration: line-through;
                color:red;
            }
            input[type=submit]{
                width:100px;
                height:30px;
                background-color: #005cbf;
                border-radius: 10px;
            }
            .pagination {
                float: right;
                margin: 0 0 5px;
            }
            .pagination li a {
                border: none;
                font-size: 13px;
                min-width: 30px;
                min-height: 30px;
                color: #999;
                margin: 0 2px;
                line-height: 30px;
                border-radius: 2px !important;
                text-align: center;
                padding: 0 6px;
            }
            .pagination li a:hover {
                color: #666;
            }
            .pagination li.active a, .pagination li.active a.page-link {
                background: #03A9F4;
            }
            .pagination li.active a:hover {
                background: #0397d6;
            }
            .pagination li.disabled i {
                color: #ccc;
            }
            .pagination li i {
                font-size: 16px;
                padding-top: 6px
            }



        </style>
        <script>
            $(document).ready(function () {
                // Activate tooltip
                $('[data-toggle="tooltip"]').tooltip();

                // Select/Deselect checkboxes
                var checkbox = $('table tbody input[type="checkbox"]');
                $("#selectAll").click(function () {
                    if (this.checked) {
                        checkbox.each(function () {
                            this.checked = true;
                        });
                    } else {
                        checkbox.each(function () {
                            this.checked = false;
                        });
                    }
                });
                checkbox.click(function () {
                    if (!this.checked) {
                        $("#selectAll").prop("checked", false);
                    }
                });
            });
        </script>

        <jsp:useBean id="a" class="dal.DAO" scope="request"></jsp:useBean>
        <jsp:useBean id="bestP" class="dal.DAO" scope="request"></jsp:useBean>
        <jsp:useBean id="newP" class="dal.DAO" scope="request"></jsp:useBean>
        </head>

        <body>
            <div class="hero_area">
                <div class="bg-box">
                    <img src="images/Nike.png" alt="">
                </div>
                <!-- header section strats -->
                <header class="header_section">
                    <div class="container">
                        <nav class="navbar navbar-expand-lg custom_nav-container ">
                            <a class="navbar-brand" href="category">
                                <span>
                                    KSHOES
                                </span>
                            </a>

                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class=""> </span>
                            </button>

                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav  mx-auto ">
                                    <li class="nav-item active">
                                        <a class="nav-link" href="category">HOME <span class="sr-only">(current)</span></a>
                                    </li>
                                <c:if test="${sessionScope.account==null}">
                                    <li class="nav-item active">
                                        <a class="nav-link" href="login">Login <span class="sr-only">(current)</span></a>
                                    </li>
                                    <li class="nav-item active">
                                        <a class="nav-link" href="Register.jsp">Register</a>    
                                    </li>
                                </c:if>


                                <c:if test="${(sessionScope.account!=null)&&(sessionScope.account.getRoldID()==2)}">

                                    <li class="nav-item active">
                                        <a class="nav-link" href="profile.jsp" onclick="show()">Profile <span class="sr-only">(current)</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="changePass.jsp">Change Password</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="logout">Logout</a>
                                    </li>
                                    <!--                                    <li class="nav-item">
                                                                            <p>Hello ${sessionScope.account. getUsername()}</p>
                                                                        </li>-->
                                </c:if>
                                <c:if test="${(sessionScope.account!=null)&&(sessionScope.account.getRoldID()==1)}">
                                    <!--                                    <li class="nav-item" >
                                                                            <h5 style="color: #FFC107">Hello ${sessionScope.account.getUsername()}</h5>
                                                                        </li>-->
                                    <li class="nav-item active">
                                        <a class="nav-link" href="profile.jsp" onclick="show()">Profile <span class="sr-only">(current)</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="changePass.jsp">Change Password</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="logout">Logout</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="list">Manager</a>
                                    </li>

                                </c:if>
                            </ul>
                            <div class="user_option" >
                                <a href="profile.jsp" class="user_link">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                </a>
                                <a class="cart_link" href="show">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 456.029 456.029" style="enable-background:new 0 0 456.029 456.029;" xml:space="preserve">
                                    <g>
                                    <g>
                                    <path d="M345.6,338.862c-29.184,0-53.248,23.552-53.248,53.248c0,29.184,23.552,53.248,53.248,53.248
                                          c29.184,0,53.248-23.552,53.248-53.248C398.336,362.926,374.784,338.862,345.6,338.862z" />
                                    </g>
                                    </g>
                                    <g>
                                    <g>
                                    <path d="M439.296,84.91c-1.024,0-2.56-0.512-4.096-0.512H112.64l-5.12-34.304C104.448,27.566,84.992,10.67,61.952,10.67H20.48
                                          C9.216,10.67,0,19.886,0,31.15c0,11.264,9.216,20.48,20.48,20.48h41.472c2.56,0,4.608,2.048,5.12,4.608l31.744,216.064
                                          c4.096,27.136,27.648,47.616,55.296,47.616h212.992c26.624,0,49.664-18.944,55.296-45.056l33.28-166.4
                                          C457.728,97.71,450.56,86.958,439.296,84.91z" />
                                    </g>
                                    </g>
                                    <g>
                                    <g>
                                    <path d="M215.04,389.55c-1.024-28.16-24.576-50.688-52.736-50.688c-29.696,1.536-52.224,26.112-51.2,55.296
                                          c1.024,28.16,24.064,50.688,52.224,50.688h1.024C193.536,443.31,216.576,418.734,215.04,389.55z" />
                                    </g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    </svg>
                                </a>
                                <!--                                <form class="form-inline" action="search" >
                                                                    <input type="text" name="search" placeholder="Search...">-->
                                <!--                                    <button class="btn  my-2 my-sm-0 nav_search-btn" type="submit">
                                                                        <i class="fa fa-search" aria-hidden="true"></i>
                                                                
                                
                                                                    </button>-->
                                <!--                                </form>-->

                                <a href="#searchproduct" data-toggle="modal">
                                    <button class="btn  my-2 my-sm-0 nav_search-btn">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </a>
                                <a href="show" class="order_online">
                                    Order Online
                                </a>
                            </div>

                        </div>
                    </nav>
                </div>
            </header>

            <div id="searchproduct" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">                   
                        <form action="search">
                            <div class="modal-header">						
                                <h4 class="modal-title">Search</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">					
                                <div >
                                    Name
                                    <input type="text" class="form-control" name="name" >
                                </div>
                                <div >
                                    From Price
                                    <input type="number" class="form-control" name="fprice" >
                                </div>
                                <div >
                                    To Price
                                    <input type="number" class="form-control" name="tprice" >
                                </div>
                                <div >
                                    Date
                                    <input type="text" class="form-control" name="date" >
                                </div>
                                <div >
                                    Color
                                    <input type="text" class="form-control" name="color" >
                                </div> 
                                <br>
                                Type Product:<select name="key" >
                                    <option  value="0">All</option>
                                    <option  value="1">Giày bóng đá</option>
                                    <option  value="2">Giày lười</option>
                                    <option  value="3">Giày da</option>
                                    <option  value="4">Giày thể thao </option>
                                    <option  value="5">Giày Sneaker</option>
                                </select>					
                            </div>
                            <div class="modal-footer">

                                <input type="submit" class="btn btn-success" value="Search">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- end header section -->
            <!-- slider section -->
            <section class="slider_section ">
                <div id="customCarousel1" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="container ">
                                <div class="row">
                                    <div class="col-md-7 col-lg-6 ">
                                        <div class="detail-box">
                                            <h1>
                                                KSHOES-Nâng tầm những bước chân!
                                            </h1>
                                            <p>
                                                KSHOES xin chào quý khách.<br/>
                                                Cảm ơn quý khách đã sử dụng sản phẩm của chúng tôi.<br/>
                                                Chúc quý khách có những trải nghiệm tuyệt vời tại KShoes  .<br/>
                                            </p>
                                            <div class="btn-box">
                                                <a href="" class="btn1">
                                                    Order Now
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item ">
                            <div class="container ">
                                <div class="row">
                                    <div class="col-md-7 col-lg-6 ">
                                        <div class="detail-box">
                                            <h1>
                                                KSHOES-BE CONFIDENT IN STEP!
                                            </h1>
                                            <p>
                                                Welcome to KSHOES.<br/>
                                                Thank you for using our service.<br/>
                                                Wish you have a great experience at KShoes.<br/>
                                            </p>
                                            <div class="btn-box">
                                                <a href="" class="btn1">
                                                    Order Now
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="container ">
                                <div class="row">
                                    <div class="col-md-7 col-lg-6 ">
                                        <div class="detail-box">
                                            <h1>
                                                Stories, style, and sporting goods at adidas, since 1949
                                            </h1>
                                            <p>
                                                Through sports, we have the power to change lives. Sports keep us fit. They keep us mindful. They bring us together. Athletes inspire us. They help us to get up and get moving. And sporting goods featuring the latest technologies help us beat our personal best. adidas is home to the runner, the basketball player, the soccer kid, the fitness enthusiast, the yogi. And even the weekend hiker looking to escape the city. The 3-Stripes are everywhere and anywhere. In sports. In music. On life’s stages. Before the whistle blows, during the race, and at the finish line. We’re here to support creators. 
                                            </p>
                                            <div class="btn-box">
                                                <a href="" class="btn1">
                                                    Order Now
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <ol class="carousel-indicators">
                            <li data-target="#customCarousel1" data-slide-to="0" class="active"></li>
                            <li data-target="#customCarousel1" data-slide-to="1"></li>
                            <li data-target="#customCarousel1" data-slide-to="2"></li>
                        </ol>
                    </div>
                </div>

            </section>
            <!-- end slider section -->
        </div>

        <!-- offer section -->
        <c:set var="b" value="${bestP.productByAmountMin}"/>

        <section class="offer_section layout_padding-bottom">
            <div class="offer_container">
                <div class="container ">
                    <div class="row">
                        <div class="col-md-6  ">
                            <div class="box ">
                                <div class="img-box">
                                    <img src="${b.getImage()}">
                                </div>
                                <div class="detail-box">
                                    <h5>
                                        Best saler: ${b.getName()}
                                    </h5>
                                    <h6>
                                        <span>
                                            <fmt:formatNumber pattern="##" value="${(b.price*2)}"/> VNÐ
                                        </span> 
                                    </h6>
                                    <a href="detail?id=${b.getProductId()}&cid=${b.getCategory_id()}">
                                        Order Now <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 456.029 456.029" style="enable-background:new 0 0 456.029 456.029;" xml:space="preserve">
                                        <g>
                                        <g>
                                        <path d="M345.6,338.862c-29.184,0-53.248,23.552-53.248,53.248c0,29.184,23.552,53.248,53.248,53.248
                                              c29.184,0,53.248-23.552,53.248-53.248C398.336,362.926,374.784,338.862,345.6,338.862z" />
                                        </g>
                                        </g>
                                        <g>
                                        <g>
                                        <path d="M439.296,84.91c-1.024,0-2.56-0.512-4.096-0.512H112.64l-5.12-34.304C104.448,27.566,84.992,10.67,61.952,10.67H20.48
                                              C9.216,10.67,0,19.886,0,31.15c0,11.264,9.216,20.48,20.48,20.48h41.472c2.56,0,4.608,2.048,5.12,4.608l31.744,216.064
                                              c4.096,27.136,27.648,47.616,55.296,47.616h212.992c26.624,0,49.664-18.944,55.296-45.056l33.28-166.4
                                              C457.728,97.71,450.56,86.958,439.296,84.91z" />
                                        </g>
                                        </g>
                                        <g>
                                        <g>
                                        <path d="M215.04,389.55c-1.024-28.16-24.576-50.688-52.736-50.688c-29.696,1.536-52.224,26.112-51.2,55.296
                                              c1.024,28.16,24.064,50.688,52.224,50.688h1.024C193.536,443.31,216.576,418.734,215.04,389.55z" />
                                        </g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <c:set var="n" value="${newP.newProduct}"/>
                        <div class="col-md-6  ">
                            <div class="box ">
                                <div class="img-box">
                                    <img src="${n.image}" alt="">
                                </div>
                                <div class="detail-box">
                                    <h5>
                                        New Product: ${n.name}
                                    </h5>
                                    <h6>
                                        <span>
                                            <fmt:formatNumber pattern="##" value="${n.price*2}"/> VNÐ
                                        </span>

                                    </h6>
                                    <a href="detail?id=${n.getProductId()}&cid=${n.getCategory_id()}">
                                        Order Now <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 456.029 456.029" style="enable-background:new 0 0 456.029 456.029;" xml:space="preserve">
                                        <g>
                                        <g>
                                        <path d="M345.6,338.862c-29.184,0-53.248,23.552-53.248,53.248c0,29.184,23.552,53.248,53.248,53.248
                                              c29.184,0,53.248-23.552,53.248-53.248C398.336,362.926,374.784,338.862,345.6,338.862z" />
                                        </g>
                                        </g>
                                        <g>
                                        <g>
                                        <path d="M439.296,84.91c-1.024,0-2.56-0.512-4.096-0.512H112.64l-5.12-34.304C104.448,27.566,84.992,10.67,61.952,10.67H20.48
                                              C9.216,10.67,0,19.886,0,31.15c0,11.264,9.216,20.48,20.48,20.48h41.472c2.56,0,4.608,2.048,5.12,4.608l31.744,216.064
                                              c4.096,27.136,27.648,47.616,55.296,47.616h212.992c26.624,0,49.664-18.944,55.296-45.056l33.28-166.4
                                              C457.728,97.71,450.56,86.958,439.296,84.91z" />
                                        </g>
                                        </g>
                                        <g>
                                        <g>
                                        <path d="M215.04,389.55c-1.024-28.16-24.576-50.688-52.736-50.688c-29.696,1.536-52.224,26.112-51.2,55.296
                                              c1.024,28.16,24.064,50.688,52.224,50.688h1.024C193.536,443.31,216.576,418.734,215.04,389.55z" />
                                        </g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section >
            <div class="offer_container">
                <div class="heading_container heading_center">
                    <h2 style="font-size: 60px">
                        Search Product
                    </h2>
                </div>
                <div class="container " style="background: #c69500 border-box " >
                    <form  id="f1"action="search">
                        <hr>
                        &nbsp;&nbsp;&nbsp;Tên sản phẩm  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;
                        Từ Giá &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                        Đến Giá &nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                        Thời gian &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                        Loại sản phẩm &nbsp;&nbsp;  Price                       
                        <br>
                        &nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="name" placeholder="Name...">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" name="fromprice" placeholder="Tu gia...">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp
                        <input type="text" name="toprice" placeholder="Den gia..."> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="type" name="date" placeholder="Ngay ban.."> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        <select name="key" >
                            <option  value="0">All</option>
                            <option  value="1">Giày bóng đá</option>
                            <option  value="2">Giày lười</option>
                            <option  value="3">Giày da</option>
                            <option  value="4">Giày thể thao </option>
                            <option  value="5">Giày Sneaker</option>                           
                        </select>

                        <select name="hide">
                            <option  value="0">All</option>
                            <option  value="1">ASC</option>
                            <option  value="2">DESC</option>
                        </select>
                        <br>
                        <br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; 
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; 
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" value="Search" style="color: chocolate">
                        <hr>
                    </form>
                </div>
            </div>
        </section>         

        <!-- end offer section -->

        <!-- food section -->

        <section class="food_section layout_padding-bottom">
            <div class="container">
                <div class="heading_container heading_center">
                    <h2 style="font-size: 60px">
                        Our Product
                    </h2>
                </div>

                <ul class="filters_menu" style="background-color: orange; font-weight: 600;
                    font-style: italic; font-family: 'Dancing Script', cursive;
                    height: 60px; font-size: 30px">
                    <li ><a href="product?cid=${0}">|All|</a></li>
                        <c:forEach items="${requestScope.category}" var="c">
                        <li class="${tag==c.getCategoryID()?"active":""}">
                            <a href="product?cid=${c.getCategoryID()}">|${c.getCateName()}|</a>
                        </li>
                    </c:forEach>
                </ul>
                <h3>${requestScope.error}</h3>

                <input type="hidden" name="num" value="1">
                <div class="filters-content">                        
                    <div class="row grid">                            
                        <c:forEach items="${requestScope.products}" var="p">
                            <div class="col-sm-6 col-lg-4">
                                <div class="box">
                                    <div>
                                        <div class="img-box">
                                            <img src="${p.getImage()}" >
                                        </div>
                                        <div class="detail-box">                                           
                                            <p>
                                                <a href="detail?id=${p.getProductId()}&cid=${p.getCategory_id()}">${p.getName()}</a>
                                            </p>

                                            <h6>
                                                Giá gốc:<span class="old">${(p.getPrice()*2)+p.getPrice()/2}</span> VNÐ
                                            </h6>
                                            <!--                                            <h6> So Luong: <input style="text-align: center" type="number" name="num" value="1" /></h6>-->
                                            <div class="options">
                                                <h5>
                                                    Sale:<fmt:formatNumber pattern="##" value="${p.price*2}"/> VNÐ
                                                </h5>
                                                <a href="buy?id=${p.getProductId()}&num=1">
                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 456.029 456.029" style="enable-background:new 0 0 456.029 456.029;" xml:space="preserve">
                                                    <g>
                                                    <g>
                                                    <path d="M345.6,338.862c-29.184,0-53.248,23.552-53.248,53.248c0,29.184,23.552,53.248,53.248,53.248
                                                          c29.184,0,53.248-23.552,53.248-53.248C398.336,362.926,374.784,338.862,345.6,338.862z" />
                                                    </g>
                                                    </g>
                                                    <g>
                                                    <g>
                                                    <path d="M439.296,84.91c-1.024,0-2.56-0.512-4.096-0.512H112.64l-5.12-34.304C104.448,27.566,84.992,10.67,61.952,10.67H20.48
                                                          C9.216,10.67,0,19.886,0,31.15c0,11.264,9.216,20.48,20.48,20.48h41.472c2.56,0,4.608,2.048,5.12,4.608l31.744,216.064
                                                          c4.096,27.136,27.648,47.616,55.296,47.616h212.992c26.624,0,49.664-18.944,55.296-45.056l33.28-166.4
                                                          C457.728,97.71,450.56,86.958,439.296,84.91z" />
                                                    </g>
                                                    </g>
                                                    <g>
                                                    <g>
                                                    <path d="M215.04,389.55c-1.024-28.16-24.576-50.688-52.736-50.688c-29.696,1.536-52.224,26.112-51.2,55.296
                                                          c1.024,28.16,24.064,50.688,52.224,50.688h1.024C193.536,443.31,216.576,418.734,215.04,389.55z" />
                                                    </g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    </svg>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>

            </div>
        </section>               

        <!--end food section--> 


        <div class="container">
            <div class="clearfix">
                <ul class="pagination">
                    <c:forEach begin="1" end="${a.totalPage}" var="l">
                        <li  class="page-item ${indexPage==l?"active":""}"><a href="paging?index=${l}" class="page-link">${l}</a></li>
                        </c:forEach>
                </ul>
            </div>
        </div>

        <!-- about section -->

        <section class="about_section layout_padding">
            <div class="container  ">
                <div class="row">
                    <div class="col-md-6 ">
                        <div class="img-box">
                            <img src="images/footerShoes.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="detail-box">
                            <div class="heading_container">
                                <h2>
                                    We Are KSHOES
                                </h2>
                            </div>
                            <p>
                                Xin cảm ơn quý khách đã tin tưởng và ủng hộ KSHOP.
                                Hi vọng KSHOES vẫn sẽ tiếp tục nhận được sự ủng hộ
                                và đánh giá của quý khách hàng trong tương lai.
                                Hãy đưa ra đánh giá và bình luận cũng như yêu cầu của quý vị
                                để chúng tôi cải thiện sản phẩm và dịch vụ để KSHOP có thể mang lại những trải nghiệm tốt nhất cho khách hàng.
                            </p>
                            <h2 style="color: pink">Be confident in step's</h2>
                            <a href="tel:0352278897">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <span>
                                    Call +84 869017965
                                </span>
                            </a>
                            <a href="mailto:hungbmhe161538@fpt.edu.vn">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                <span>
                                    khangtdhe163798@fpt.edu.vn
                                </span>
                            </a>
                            <div class="footer_social">
                                <a href="https://www.facebook.com/flypainn/">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </a>
                                <a href="https://www.tiktok.com/@finalleaf">
                                    <i class="fa fa-google" aria-hidden="true"></i>
                                </a>

                                <a href="https://www.instagram.com/24thang.10/?hl=vi/">
                                    <i class="fa fa-instagram" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <!-- end about section -->

        <!-- book section -->
        <!--        <section class="book_section layout_padding">
                    <div class="container">
                        <div class="heading_container">
                            <h2>
                                Book A Table
                            </h2>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form_container">
                                    <form action="">
                                        <div>
                                            <input type="text" class="form-control" placeholder="Your Name" />
                                        </div>
                                        <div>
                                            <input type="text" class="form-control" placeholder="Phone Number" />
                                        </div>
                                        <div>
                                            <input type="email" class="form-control" placeholder="Your Email" />
                                        </div>
                                        <div>
                                            <select class="form-control nice-select wide">
                                                <option value="" disabled selected>
                                                    How many persons?
                                                </option>
                                                <option value="">
                                                    2
                                                </option>
                                                <option value="">
                                                    3
                                                </option>
                                                <option value="">
                                                    4
                                                </option>
                                                <option value="">
                                                    5
                                                </option>
                                            </select>
                                        </div>
                                        <div>
                                            <input type="date" class="form-control">
                                        </div>
                                        <div class="btn_box">
                                            <button>
                                                Book Now
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="map_container ">
                                    <div id="googleMap"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>-->
        <!-- end book section -->

        <!-- client section -->

        <!--        <section class="client_section layout_padding-bottom">
                    <div class="container">
                        <div class="heading_container heading_center psudo_white_primary mb_45">
                            <h2>
                                What Says Our Customers
                            </h2>
                        </div>
                        <div class="carousel-wrap row ">
                            <div class="owl-carousel client_owl-carousel">
                                <div class="item">
                                    <div class="box">
                                        <div class="detail-box">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
                                            </p>
                                            <h6>
                                                Moana Michell
                                            </h6>
                                            <p>
                                                magna aliqua
                                            </p>
                                        </div>
                                        <div class="img-box">
                                            <img src="images/client1.jpg" alt="" class="box-img">
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="box">
                                        <div class="detail-box">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
                                            </p>
                                            <h6>
                                                Mike Hamell
                                            </h6>
                                            <p>
                                                magna aliqua
                                            </p>
                                        </div>
                                        <div class="img-box">
                                            <img src="images/client2.jpg" alt="" class="box-img">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>-->
        <!--</section>-->

        <!--end client section--> 

        <!--footer section--> 
        <!--        <footer class="footer_section">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4 footer-col">
                                <div class="footer_contact">
                                    <h4>
                                        Contact Us
                                    </h4>
                                    <div class="contact_link_box">
                                        <a href="">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                            <span>
                                                Location
                                            </span>
                                        </a>
                                        <a href="">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                            <span>
                                                Call +01 1234567890
                                            </span>
                                        </a>
                                        <a href="">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <span>
                                                demo@gmail.com
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 footer-col">
                                <div class="footer_detail">
                                    <a href="" class="footer-logo">
                                        Feane
                                    </a>
                                    <p>
                                        Necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with
                                    </p>
                                    <div class="footer_social">
                                        <a href="">
                                            <i class="fa fa-facebook" aria-hidden="true"></i>
                                        </a>
                                        <a href="">
                                            <i class="fa fa-twitter" aria-hidden="true"></i>
                                        </a>
                                        <a href="">
                                            <i class="fa fa-linkedin" aria-hidden="true"></i>
                                        </a>
                                        <a href="">
                                            <i class="fa fa-instagram" aria-hidden="true"></i>
                                        </a>
                                        <a href="">
                                            <i class="fa fa-pinterest" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 footer-col">
                                <h4>
                                    Opening Hours
                                </h4>
                                <p>
                                    Everyday
                                </p>
                                <p>
                                    10.00 Am -10.00 Pm
                                </p>
                            </div>
                        </div>
                        <div class="footer-info">
                            <p>
                                &copy; <span id="displayYear"></span> All Rights Reserved By
                                <a href="https://html.design/">Free Html Templates</a><br><br>
                                &copy; <span id="displayYear"></span> Distributed By
                                <a href="https://themewagon.com/" target="_blank">ThemeWagon</a>
                            </p>
                        </div>
                    </div>
                </footer>-->
        <!--footer section 
    
        <!-- jQery -->
        <script src="js/jquery-3.4.1.min.js"></script>
        <!-- popper js -->
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
        </script>
        <!-- bootstrap js -->
        <script src="js/bootstrap.js"></script>
        <!-- owl slider -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js">
        </script>
        <!-- isotope js -->
        <script src="https://unpkg.com/isotope-layout@3.0.4/dist/isotope.pkgd.min.js"></script>
        <!-- nice select -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/js/jquery.nice-select.min.js"></script>
        <!-- custom js -->
        <script src="js/custom.js"></script>
        <!-- Google Map -->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh39n5U-4IoWpsVGUHWdqB6puEkhRLdmI&callback=myMap">
        </script>
        <!-- End Google Map -->

    </body>

</html>
<script type="text/javascript">
//    function buy(id) {
//        var m = document.f.num.value;
//        document.f.action = "buy?id=" + id + "&num=" + m;
//        document.f.submit();
//    }

    function change() {
        document.getElementById("f1").submit();

    }


</script>
