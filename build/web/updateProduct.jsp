<%-- 
    Document   : Register
    Created on : Oct 15, 2022, 6:53:30 PM
    Author     : User
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="ckeditor/ckeditor.js"></script>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <title>Kshoes</title>
        <link rel="stylesheet" href="./css/login.css"/>

    </head>
    <body>

        <div class="login-wrap">
            <div class="login-html">
                <input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab">Update </label>

                <!--                <h2>Register</h2>-->
                <c:if test="${requestScope.u==1}">
                    <div class="login-form">
                        <c:set var="p" value="${requestScope.product}"/>
                        <form action="update" method="post">
                            <div class="sign-up-htm">
                                <div class="group">
                                    <label for="productid" class="label">ProductId</label>
                                    <input  readonly name="pid" type="text" value="${p.getProductId()}"class="input">
                                </div>
                                <div class="group">
                                    <label for="name" class="label">Name</label>
                                    <input  name="pname" type="text" value="${p.getName()}"class="input">
                                </div>
                                <div class="group">
                                    <label for="image" class="label">Image</label>
                                    <input  name="image" type="text" value="${p.getImage()}"class="input">
                                </div>
                                <div class="group">
                                    <label for="price" class="label">Price</label>
                                    <input  name="price" type="text" value="${p.getPrice()}"class="input">
                                </div>

                                <div class="group">
                                    <label for="amount" class="label">Amount</label>
                                    <input  name="amount" type="text" value="${p.getAmount()}" class="input">
                                </div>
                                <div class="group">
                                    <label for="sell date" class="label">Sell Date</label>
                                    <input  name="date" type="text" value="${p.getSell_date()}"class="input">
                                </div>
                                <input type="hidden" name="hide" value="1">
                                <div class="group">
                                    <input type="submit" class="button" value="Update">
                                </div>
                                <div class="hr"></div>
                            </div>
                        </form>
                    </div>
                </c:if>
                <c:if test="${requestScope.u==2}">
                    <div class="login-form">
                        <c:set var="u" value="${requestScope.user}"/>
                        <form action="update" method="post">
                            <div class="sign-up-htm">
                                <div class="group">
                                    <label for="userid" class="label">UserId</label>
                                    <input    readonly name="uid" type="text" value="${u.getUserID()}"class="input">
                                </div>
                                <div class="group">
                                    <label for="name" class="label">Name</label>
                                    <input   readonly name="uname" type="text" value="${u.getUsername()}"class="input">
                                </div>
                                <div class="group">
                                    <label for="gender" class="label">Gender</label>
                                    <input   readonly name="gender" type="text" value="${u.getGender()}"class="input">
                                </div>
                                <div class="group">
                                    <label for="phone" class="label">Phone</label>
                                    <input   readonly name="phone" type="text" value="${u.getPhone()}"class="input">
                                </div>
                                <div class="group">
                                    <label for="email" class="label">Email</label>
                                    <input   readonly name="email" type="text" value="${u.getEmail()}"class="input">
                                </div>
                                <div class="group">
                                    <label for="address" class="label">Address</label>
                                    <input   readonly name="address" type="text" value="${u.getAddress()}"class="input">
                                </div>
                                <div class="group">
                                    <label for="password" class="label">Password</label>
                                    <input   readonly name="password" type="text" value="${u.getPassword()}"class="input">
                                </div>
                                <div class="group">
                                    <label for="roleid" class="label">RoleId</label>
                                    <input   name="role" type="text" value="${u.getRoldID()}"class="input">
                                </div>
                                <input type="hidden" name="hide" value="2">
                                <div class="group">
                                    <input type="submit" class="button" value="Update">
                                </div>
                                <div class="hr"></div>
                            </div>
                        </form>
                    </div>
                </c:if>
                <c:if test="${requestScope.u==3}">
                    <div class="login-form">
                        <c:set var="c" value="${requestScope.category}"/>
                        <form action="update" method="post">
                            <div class="sign-up-htm">
                                <div class="group">
                                    <label for="categoryid" class="label">CategoryId</label>
                                    <input   name="cid" type="text" value="${c.getCategoryID()}"class="input">
                                </div>
                                <div class="group">
                                    <label for="catename" class="label">CateName</label>
                                    <input  name="cname" type="text" value="${c.getCateName()}"class="input">
                                </div>
                                <div class="group">
                                    <label for="supplierid" class="label">SupplierID</label>
                                    <input  name="supplierid" type="text" value="${c.getSupplier_id()}"class="input">
                                </div>

                                <input type="hidden" name="hide" value="3">
                                <div class="group">
                                    <input type="submit" class="button" value="Update">
                                </div>
                                <div class="hr"></div>
                            </div>
                        </form>
                    </div>
                </c:if>
                <c:if test="${requestScope.u==4}">
                    <div class="login-form">
                        <c:set var="s" value="${requestScope.supplier}"/>
                        <form action="update" method="post">
                            <div class="sign-up-htm">
                                <div class="group">
                                    <label for="supplierid" class="label">SupplierId</label>
                                    <input   name="sid" type="text" value="${s.getId()}"class="input">
                                </div>
                                <div class="group">
                                    <label for="name" class="label">Name</label>
                                    <input  name="sname" type="text" value="${s.getName()}"class="input">
                                </div>
                                <div class="group">
                                    <label for="address" class="label">Address</label>
                                    <input  name="saddress" type="text" value="${s.getAddress()}"class="input">
                                </div>
                                <div class="group">
                                    <label for="phone" class="label">Phone</label>
                                    <input  name="sphone" type="text" value="${s.getPhone()}"class="input">
                                </div>
                                <input type="hidden" name="hide" value="4">
                                <div class="group">
                                    <input type="submit" class="button" value="Update">
                                </div>
                                <div class="hr"></div>
                            </div>
                        </form>
                    </div>
                </c:if>
            </div>
        </div>
    </body>


   
</html>
