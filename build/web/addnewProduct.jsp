<%-- 
    Document   : addnewProduct
    Created on : Oct 28, 2022, 10:57:15 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<script src="ckeditor/ckeditor.js"></script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Kshoes</title>
        <link rel="stylesheet" href="./css/login.css"/>

    </head>
    <body>

        <div class="login-wrap">
            <div class="login-html">
                <input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab">Add New Product</label>
                
<!--                <h2>Register</h2>-->
                <div class="login-form">
                    <h3 style="color: red">${requestScope.ms}</h3>
                    <form action="add">                        
                        <div class="sign-up-htm">
                            <div class="group">
                                <label for="userid" class="label">ProductId</label>
                                <input  name="id" type="text" class="input">
                            </div>
                            <div class="group">
                                <label for="user" class="label">Name</label>
                                <input  name="name" id="editor1" type="text" class="input">
                            </div>
                            <div class="group">
                                <label for="pass" class="label">Image</label>
                                <input  name="image" type="text" class="input" >
                            </div>
                            <div class="group">
                                <label for="price" class="label">Price</label>
                                <input  name="price" type="text" class="input" >
                            </div>
                             <div class="group">
                                <label for="amount" class="label">Amount</label>
                                <input name="amount" type="text" class="input">
                            </div>
                             <div class="group">
                                <label for="sell date" class="label">Sell Date</label>
                                <input  name="date" type="text" class="input">
                            </div>                             
                            <div class="group">
                                <input type="submit" class="button" value="Add">
                            </div>
                            <input type="hidden" name="add" value="1">
                            <div class="hr"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
    
    
     <script>
                // Replace the <textarea id="editor1"> with a CKEditor 4
                // instance, using default configuration.
                CKEDITOR.replace( 'editor1' );
            </script>
</html>

