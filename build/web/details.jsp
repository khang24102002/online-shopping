<%-- 
    Document   : details
    Created on : Oct 17, 2022, 9:08:47 PM
    Author     : User
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!doctype html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>KHSOES</title>
        <link href='https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css' rel='stylesheet'>
        <link href='' rel='stylesheet'>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <!-- Site Metas -->
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <link rel="shortcut icon" href="images/LogoShoes.png" type="">

        <title> KSHOES </title>

        <!-- bootstrap core css -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />

        <!--owl slider stylesheet -->
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />
        <!-- nice select  -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/css/nice-select.min.css" integrity="sha512-CruCP+TD3yXzlvvijET8wV5WxxEh5H8P4cmz0RFbKK6FlZ2sYl3AEsKlLPHbniXKSrDdFewhbmBK5skbdsASbQ==" crossorigin="anonymous" />
        <!-- font awesome style -->
        <link href="css/font-awesome.min.css" rel="stylesheet" />

        <!-- Custom styles for this template -->
        <link href="css/style.css" rel="stylesheet" />
        <!-- responsive style -->
        <link href="css/responsive.css" rel="stylesheet" />
        <style>body {
                font-family: 'Roboto Condensed', sans-serif;
                background-color: #f5f5f5
            }

            .hedding {
                font-size: 20px;
                color: #ab8181 ;
            }

            .main-section {
                position: absolute;
                left: 50%;
                right: 50%;
                transform: translate(-50%, 5%);
            }

            .left-side-product-box img {
                width: 100%;
            }

            .left-side-product-box .sub-img img {
                margin-top: 5px;
                width: 83px;
                height: 100px;
            }

            .right-side-pro-detail span {
                font-size: 15px;
            }

            .right-side-pro-detail p {
                font-size: 25px;
                color: #a1a1a1;
            }

            .right-side-pro-detail .price-pro {
                color: #E45641;
            }

            .right-side-pro-detail .tag-section {
                font-size: 18px;
                color: #5D4C46;
            }

            .pro-box-section .pro-box img {
                width: 100%;
                height: 200px;
            }

            @media (min-width:360px) and (max-width:640px) {
                .pro-box-section .pro-box img {
                    height: auto;
                }
            }</style>
        <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
        <script type='text/javascript' src='https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js'></script>
        <script type='text/javascript' src='https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js'></script>
    </head>
    <body>
        <header class="header_section" style="background: black">
            <div class="container">
                <nav class="navbar navbar-expand-lg custom_nav-container ">
                    <a class="navbar-brand" href="category">
                        <span>
                            Kshoes
                        </span>
                    </a>

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class=""> </span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav  mx-auto ">
                            <li class="nav-item active">
                                <a class="nav-link" href="category">HOME <span class="sr-only">(current)</span></a>
                            </li>
                            <c:if test="${sessionScope.account==null}">
                                <li class="nav-item active">
                                    <a class="nav-link" href="login.jsp">Login <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link" href="Register.jsp">Register</a>    
                                </li>
                            </c:if>


                            <c:if test="${(sessionScope.account!=null)&&(sessionScope.account.getRoldID()==2)}">

                                <li class="nav-item active">
                                    <a class="nav-link" href="profile.jsp" onclick="show()">Profile <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="changePass.jsp">Change Password</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="logout">Logout</a>
                                </li>
                            </c:if>
                            <c:if test="${(sessionScope.account!=null)&&(sessionScope.account.getRoldID()==1)}">
                                <li class="nav-item active">
                                    <a class="nav-link" href="profile.jsp" onclick="show()">Profile <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="changePass.jsp">Change Password</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="logout">Logout</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="list">Manager</a>
                                </li>
                            </c:if>

                        </ul>
                        <div class="user_option">
                            <a href="profile.jsp" class="user_link">
                                <i class="fa fa-user" aria-hidden="true"></i>
                            </a>
                            <a class="cart_link" href="show">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 456.029 456.029" style="enable-background:new 0 0 456.029 456.029;" xml:space="preserve">
                                <g>
                                <g>
                                <path d="M345.6,338.862c-29.184,0-53.248,23.552-53.248,53.248c0,29.184,23.552,53.248,53.248,53.248
                                      c29.184,0,53.248-23.552,53.248-53.248C398.336,362.926,374.784,338.862,345.6,338.862z" />
                                </g>
                                </g>
                                <g>
                                <g>
                                <path d="M439.296,84.91c-1.024,0-2.56-0.512-4.096-0.512H112.64l-5.12-34.304C104.448,27.566,84.992,10.67,61.952,10.67H20.48
                                      C9.216,10.67,0,19.886,0,31.15c0,11.264,9.216,20.48,20.48,20.48h41.472c2.56,0,4.608,2.048,5.12,4.608l31.744,216.064
                                      c4.096,27.136,27.648,47.616,55.296,47.616h212.992c26.624,0,49.664-18.944,55.296-45.056l33.28-166.4
                                      C457.728,97.71,450.56,86.958,439.296,84.91z" />
                                </g>
                                </g>
                                <g>
                                <g>
                                <path d="M215.04,389.55c-1.024-28.16-24.576-50.688-52.736-50.688c-29.696,1.536-52.224,26.112-51.2,55.296
                                      c1.024,28.16,24.064,50.688,52.224,50.688h1.024C193.536,443.31,216.576,418.734,215.04,389.55z" />
                                </g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                </svg>
                            </a>
                            <form class="form-inline">
                                <button class="btn  my-2 my-sm-0 nav_search-btn" type="submit">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </button>
                            </form>
                            <a href="show" class="order_online">
                                Order Online
                            </a>
                        </div>

                    </div>
                </nav>
            </div>
        </header>
        <div oncontextmenu='return false' class='snippet-body'>
            <div class="container">
                <div class="col-lg-8 border p-3 main-section bg-white">
                    <div class="row hedding m-0 pl-3 pt-0 pb-3">
                        Products Details
                    </div>
                    <c:set var="d" value="${requestScope.detail}"/>

                    <div class="row m-0">
                        <div class="col-lg-4 left-side-product-box pb-3">
                            <img src="${d.getImage()}" class="border p-3">
                            <h1 style="text-align: center">${d.getName()}</h1>
                        </div>
                        <div class="col-lg-8">
                            <div class="right-side-pro-detail border p-3 m-0">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <span>Amount</span>
                                        <p class="m-0 p-0">${d.getAmount()}</p>
                                    </div>
                                    <div class="col-lg-12">
                                        <p class="m-0 p-0 price-pro">${d.getPrice()*2}đ</p>
                                        <hr class="p-0 m-0">
                                    </div>

                                    <div class="col-lg-12 pt-2">
                                        <h5>Description:</h5>
                                        <span>${d.getDescription()}</span>
                                        <hr class="m-0 pt-2 mt-2">
                                    </div>                                  
                                    <div class="col-lg-12" >
                                        Color:${d.getColor()} <br>
                                        Sell date: ${d.getSell_date()}
                                        <br>

                                        <ul>Feedback:
                                            <c:forEach items="${requestScope.feedback}" var="f">
                                                <li>${f.getContent()}</li>
                                                </c:forEach>                                           
                                        </ul> 
                                    </div>                                 

                                    <div class="col-lg-12 mt-3">
                                        <div class="row">
                                            <div class="col-lg-6 pb-2">
                                                <a href="buy?id=${d.getProductId()}&num=1" class="btn btn-danger w-100">Add To Cart</a>
                                            </div>
                                            <div class="col-lg-6">
                                                <a href="category" class="btn btn-success w-100">Shop Now</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>                       
                        <form action="feedback">
                            Danh gia san pham:<input type="text" name="fb"><br>
                            <input type="hidden" name="pid" value="${d.getProductId()}">
                            <input type="submit" value="Add">
                        </form>
                    </div>                    
                    <br>
                    <h3 style="text-align: center">Sample Product</h3>
                    <hr>
                    <div>
                        <ul class="filters_menu">
                            <c:forEach items="${requestScope.sampleP}" var="sample">
                                <li style="display: flex"> 
                                    <img src="${sample.image}" alt="" width="80px" height="80px"> 
                                    &nbsp;&nbsp;&nbsp; <a href="detail?id=${sample.getProductId()}&cid=${sample.getCategory_id()}"> <p>${sample.name}</p></a>

                                </li>  
                            </c:forEach>                                               
                        </ul>
                    </div>    
                </div>

            </div>
        </div>



    </body>
</html>
