<%-- 
    Document   : Register
    Created on : Oct 15, 2022, 6:53:30 PM
    Author     : User
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Kshoes</title>
        <link rel="stylesheet" href="./css/login.css"/>

    </head>
    <body>

        <div class="login-wrap">
            <div class="login-html">
                <input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab">Register</label>
                
<!--                <h2>Register</h2>-->
                <div class="login-form">
                    <h3 style="color: red">${requestScope.ms}</h3>
                    <form action="register">
                        
                        <div class="sign-up-htm">
                            <div class="group">
                                <label for="userid" class="label">UserID</label>
                                <input id="userid" name="id" type="text" class="input">
                            </div>
                            <div class="group">
                                <label for="user" class="label">Username</label>
                                <input id="user" name="user" type="text" class="input">
                            </div>
                            <div class="group">
                                <label for="pass" class="label">Password</label>
                                <input id="pass" name="pass" type="password" class="input" data-type="password">
                            </div>
                            <div class="group">
                                <label for="pass" class="label">Confirm Password</label>
                                <input id="pass" name="rpass" type="password" class="input" data-type="password">
                            </div>
                            <div class="group">
                                <label for="email" class="label">Email </label>
                                <input id="pass" name="email" type="text" class="input">
                            </div>
                            <div class="group">
                                <input type="submit" class="button" value="Sign Up">
                            </div>
                            <div class="hr"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
