<%-- 
    Document   : manager
    Created on : Oct 16, 2022, 1:17:47 PM
    Author     : User
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>KHSOES</title>
        <link href='https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css' rel='stylesheet'>
        <link href='' rel='stylesheet'>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <!-- Site Metas -->
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <link rel="shortcut icon" href="images/LogoShoes.png" type="">



        <!-- bootstrap core css -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />

        <!--owl slider stylesheet -->
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />
        <!-- nice select  -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/css/nice-select.min.css" integrity="sha512-CruCP+TD3yXzlvvijET8wV5WxxEh5H8P4cmz0RFbKK6FlZ2sYl3AEsKlLPHbniXKSrDdFewhbmBK5skbdsASbQ==" crossorigin="anonymous" />
        <!-- font awesome style -->
        <link href="css/font-awesome.min.css" rel="stylesheet" />

        <!-- Custom styles for this template -->
        <link href="css/style.css" rel="stylesheet" />
        <!-- responsive style -->
        <link href="css/responsive.css" rel="stylesheet" />
        <style>
            body {
                color: #566787;
                background: #f5f5f5;
                font-family: 'Varela Round', sans-serif;
                font-size: 13px;
            }
            .table-responsive {
                margin: 30px 0;
            }
            .table-wrapper {
                background: #fff;
                padding: 20px 25px;
                border-radius: 3px;
                min-width: 1000px;
                box-shadow: 0 1px 1px rgba(0,0,0,.05);
            }
            .table-title {
                padding-bottom: 15px;
                background: #435d7d;
                color: #fff;
                padding: 16px 30px;
                min-width: 100%;
                margin: -20px -25px 10px;
                border-radius: 3px 3px 0 0;
            }
            .table-title h2 {
                margin: 5px 0 0;
                font-size: 24px;
            }
            .table-title .btn-group {
                float: right;
            }
            .table-title .btn {
                color: #fff;
                float: right;
                font-size: 13px;
                border: none;
                min-width: 50px;
                border-radius: 2px;
                border: none;
                outline: none !important;
                margin-left: 10px;
            }
            .table-title .btn i {
                float: left;
                font-size: 21px;
                margin-right: 5px;
            }
            .table-title .btn span {
                float: left;
                margin-top: 2px;
            }
            table.table tr th, table.table tr td {
                border-color: #e9e9e9;
                padding: 12px 15px;
                vertical-align: middle;
            }
            table.table tr th:first-child {
                width: 60px;
            }
            table.table tr th:last-child {
                width: 100px;
            }
            table.table-striped tbody tr:nth-of-type(odd) {
                background-color: #fcfcfc;
            }
            table.table-striped.table-hover tbody tr:hover {
                background: #f5f5f5;
            }
            table.table th i {
                font-size: 13px;
                margin: 0 5px;
                cursor: pointer;
            }
            table.table td:last-child i {
                opacity: 0.9;
                font-size: 22px;
                margin: 0 5px;
            }
            table.table td a {
                font-weight: bold;
                color: #566787;
                display: inline-block;
                text-decoration: none;
                outline: none !important;
            }
            table.table td a:hover {
                color: #2196F3;
            }
            table.table td a.edit {
                color: #FFC107;
            }
            table.table td a.delete {
                color: #F44336;
            }
            table.table td i {
                font-size: 19px;
            }
            table.table .avatar {
                border-radius: 50%;
                vertical-align: middle;
                margin-right: 10px;
            }
            .pagination {
                float: right;
                margin: 0 0 5px;
            }
            .pagination li a {
                border: none;
                font-size: 13px;
                min-width: 30px;
                min-height: 30px;
                color: #999;
                margin: 0 2px;
                line-height: 30px;
                border-radius: 2px !important;
                text-align: center;
                padding: 0 6px;
            }
            .pagination li a:hover {
                color: #666;
            }
            .pagination li.active a, .pagination li.active a.page-link {
                background: #03A9F4;
            }
            .pagination li.active a:hover {
                background: #0397d6;
            }
            .pagination li.disabled i {
                color: #ccc;
            }
            .pagination li i {
                font-size: 16px;
                padding-top: 6px
            }
            .hint-text {
                float: left;
                margin-top: 10px;
                font-size: 13px;
            }
            /* Custom checkbox */
            .custom-checkbox {
                position: relative;
            }
            .custom-checkbox input[type="checkbox"] {
                opacity: 0;
                position: absolute;
                margin: 5px 0 0 3px;
                z-index: 9;
            }
            .custom-checkbox label:before{
                width: 18px;
                height: 18px;
            }
            .custom-checkbox label:before {
                content: '';
                margin-right: 10px;
                display: inline-block;
                vertical-align: text-top;
                background: white;
                border: 1px solid #bbb;
                border-radius: 2px;
                box-sizing: border-box;
                z-index: 2;
            }
            .custom-checkbox input[type="checkbox"]:checked + label:after {
                content: '';
                position: absolute;
                left: 6px;
                top: 3px;
                width: 6px;
                height: 11px;
                border: solid #000;
                border-width: 0 3px 3px 0;
                transform: inherit;
                z-index: 3;
                transform: rotateZ(45deg);
            }
            .custom-checkbox input[type="checkbox"]:checked + label:before {
                border-color: #03A9F4;
                background: #03A9F4;
            }
            .custom-checkbox input[type="checkbox"]:checked + label:after {
                border-color: #fff;
            }
            .custom-checkbox input[type="checkbox"]:disabled + label:before {
                color: #b8b8b8;
                cursor: auto;
                box-shadow: none;
                background: #ddd;
            }
            /* Modal styles */
            .modal .modal-dialog {
                max-width: 400px;
            }
            .modal .modal-header, .modal .modal-body, .modal .modal-footer {
                padding: 20px 30px;
            }
            .modal .modal-content {
                border-radius: 3px;
                font-size: 14px;
            }
            .modal .modal-footer {
                background: #ecf0f1;
                border-radius: 0 0 3px 3px;
            }
            .modal .modal-title {
                display: inline-block;
            }
            .modal .form-control {
                border-radius: 2px;
                box-shadow: none;
                border-color: #dddddd;
            }
            .modal textarea.form-control {
                resize: vertical;
            }
            .modal .btn {
                border-radius: 2px;
                min-width: 100px;
            }
            .modal form label {
                font-weight: normal;
            }
            .menu{
                background-color: #c8cbcf;
                height: 50px;
               
            }
            .menu li{
                text-align: center;
                list-style: none;
            }

        </style>
        <script>
            $(document).ready(function () {
                // Activate tooltip
                $('[data-toggle="tooltip"]').tooltip();

                // Select/Deselect checkboxes
                var checkbox = $('table tbody input[type="checkbox"]');
                $("#selectAll").click(function () {
                    if (this.checked) {
                        checkbox.each(function () {
                            this.checked = true;
                        });
                    } else {
                        checkbox.each(function () {
                            this.checked = false;
                        });
                    }
                });
                checkbox.click(function () {
                    if (!this.checked) {
                        $("#selectAll").prop("checked", false);
                    }
                });
            });
            function doDelete(id) {
                if (confirm("Are you sure to delete product with id=" + id)) {
                    window.location = "delete?id=" + id;
                }
            }
            function doDeleteS(id) {
                if (confirm("Are you sure to delete product with id=" + id)) {
                    window.location = "deletes?id=" + id;
                }
            }
            function doDeleteC(id) {
                if (confirm("Are you sure to delete product with id=" + id)) {
                    window.location = "deletec?id=" + id;
                }
            }
        </script>
    </head>
    <jsp:useBean id="a" class="dal.DAO" scope="request"></jsp:useBean>
        <body>
            <header class="header_section" style="background: black">
                <div class="container">
                    <nav class="navbar navbar-expand-lg custom_nav-container ">
                        <a class="navbar-brand" href="homepage">
                            <span style="color: red">
                                Kshoes
                            </span>
                        </a>

                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class=""> </span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav  mx-auto ">
                                <li class="nav-item active">
                                    <a class="nav-link" href="category">HOME <span class="sr-only">(current)</span></a>
                                </li>
                            <c:if test="${sessionScope.account==null}">
                                <li class="nav-item active">
                                    <a class="nav-link" href="login.jsp">Login <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link" href="Register.jsp">Register</a>    
                                </li>
                            </c:if>


                            <c:if test="${(sessionScope.account!=null)&&(sessionScope.account.getRoldID()==2)}">

                                <li class="nav-item active">
                                    <a class="nav-link" href="profile.jsp" onclick="show()">Profile <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link" href="changePass.jsp">Change Password</a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link" href="logout">Logout</a>
                                </li>
                            </c:if>
                            <c:if test="${(sessionScope.account!=null)&&(sessionScope.account.getRoldID()==1)}">
                                <li class="nav-item active">
                                    <a class="nav-link" href="profile.jsp" onclick="show()">Profile <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link" href="changePass.jsp">Change Password</a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link" href="logout">Logout</a>
                                </li>                               
                                <li class="nav-item active">
                                    <a class="nav-link" href="statis?active=1">Statistical</a>
                                </li>

                            </c:if>

                        </ul>
                        <div class="user_option">
                            <a href="profile.jsp" class="user_link">
                                <i class="fa fa-user" aria-hidden="true"></i>
                            </a>
                            <a class="cart_link" href="show">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 456.029 456.029" style="enable-background:new 0 0 456.029 456.029;" xml:space="preserve">
                                <g>
                                <g>
                                <path d="M345.6,338.862c-29.184,0-53.248,23.552-53.248,53.248c0,29.184,23.552,53.248,53.248,53.248
                                      c29.184,0,53.248-23.552,53.248-53.248C398.336,362.926,374.784,338.862,345.6,338.862z" />
                                </g>
                                </g>
                                <g>
                                <g>
                                <path d="M439.296,84.91c-1.024,0-2.56-0.512-4.096-0.512H112.64l-5.12-34.304C104.448,27.566,84.992,10.67,61.952,10.67H20.48
                                      C9.216,10.67,0,19.886,0,31.15c0,11.264,9.216,20.48,20.48,20.48h41.472c2.56,0,4.608,2.048,5.12,4.608l31.744,216.064
                                      c4.096,27.136,27.648,47.616,55.296,47.616h212.992c26.624,0,49.664-18.944,55.296-45.056l33.28-166.4
                                      C457.728,97.71,450.56,86.958,439.296,84.91z" />
                                </g>
                                </g>
                                <g>
                                <g>
                                <path d="M215.04,389.55c-1.024-28.16-24.576-50.688-52.736-50.688c-29.696,1.536-52.224,26.112-51.2,55.296
                                      c1.024,28.16,24.064,50.688,52.224,50.688h1.024C193.536,443.31,216.576,418.734,215.04,389.55z" />
                                </g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                </svg>
                            </a>
                            <form class="form-inline">
                                <button class="btn  my-2 my-sm-0 nav_search-btn" type="submit">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </button>
                            </form>
                            <a href="show" class="order_online">
                                Order Online
                            </a>
                        </div>

                    </div>
                </nav>
            </div>
        </header>
        <div class="container-xl">
            <ul class="menu" style="display: flex">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <li><a href="manager?action=1">Manager Product</a></li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <li><a href="manager?action=2">Manager User</a></li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <li><a href="manager?action=3">Manager Category</a></li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <li><a href="manager?action=4">Manager Supplier</a></li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

            </ul>
        </div>
        <c:if test="${requestScope.action==1}">
            <div class="container-xl">
                <div class="table-responsive">
                    <div class="table-wrapper">
                        <div class="table-title">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h2>Manager <b>Products</b></h2>

                                    <h4><a href="category" class="fa fa-arrow-circle-left">Back</a></h4>
                                </div>
                                <div class="col-sm-6">
                                    <a href="addnewProduct.jsp" class="btn btn-success" ><i class="material-icons">&#xE147;</i> <span>Add New Product</span></a>				
                                    <!--                                    data-toggle="modal"-->
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>                                                            
                                    <th>ProductID</th>
                                    <th>Name</th>   
                                    <th>Image</th>
                                    <th>Price</th>                              
                                    <th>Amount</th>
                                    <th>Sell Date</th>
                                    <th>Actions</th>
                                </tr>

                            </thead>
                            <tbody>
                                <c:forEach items="${requestScope.allproduct}" var="p">
                                    <tr>
                                        <td>${p.getProductId()}</td>
                                        <td>${p.getName()}</td> 
                                        <td>
                                            <img src="${p.getImage()}" width="80px"height="80px">
                                        </td> 
                                        <td>${p.getPrice() }</td>                                   
                                        <td>${p.getAmount()}</td>
                                        <td>${p.getSell_date()}</td>
                                        <td>
                                            <!--                                        data-toggle="modal"-->
                                            <a href="update?id=${p.getProductId()}&u=1"  class="edit" ><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                                            <a href="#" onclick="doDelete('${p.getProductId()}')" class="delete" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
                                        </td>
                                    </tr>
                                </c:forEach>                          
                            </tbody>
                        </table>

                        <div class="clearfix">
                            <ul class="pagination">
                                <c:forEach begin="1" end="${a.totalPage}" var="l">
                                    <li class="page-item ${indexPage==l?"active":""}"><a href="pagingmanager?index=${l}&action=1" class="page-link">${l}</a></li>
                                    </c:forEach>
                            </ul>
                        </div>

                    </div>
                </div>        
            </div>
        </c:if>
        <c:if test="${requestScope.action==2}">
            <div class="container-xl">
                <div class="table-responsive">
                    <div class="table-wrapper">
                        <div class="table-title">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h2>Manager <b>User</b></h2>

                                    <h4><a href="category" class="fa fa-arrow-circle-left">Back</a></h4>
                                </div>
                                <!--                                <div class="col-sm-6">
                                                                    <a href="addnewProduct.jsp" class="btn btn-success" ><i class="material-icons">&#xE147;</i> <span>Add New Product</span></a>				
                                                                                                        data-toggle="modal"
                                                                </div>-->
                            </div>
                        </div>
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>                                                            
                                    <th>UserID</th>
                                    <th>Name</th>   
                                    <th>Gender</th>
                                    <th>Phone</th>                              
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Password</th>
                                    <th>RoleID</th>
                                    <th>Action</th>    
                                </tr>

                            </thead>
                            <tbody>
                                <c:forEach items="${requestScope.alluser}" var="u">
                                    <tr>
                                        <td>${u.getUserID()}</td>
                                        <td>${u.getUsername()}</td> 
                                        <td>

                                            <c:if test="${u.getGender()=='nam'}">
                                                <img src="images/female.png" width="80px"height="80px">
                                            </c:if>
                                            <c:if test="${u.getGender()=='nu'}">
                                                <img src="images/male.png" width="80px"height="80px">
                                            </c:if>
                                        </td> 
                                        <td>${u.getPhone() }</td>                                   
                                        <td>${u.getEmail() }</td>
                                        <td>${u.getAddress()}</td>
                                        <td>${u.getPassword()}</td>
                                        <td>${u.getRoldID()}</td>
                                        <td>
                                            <!--                                        data-toggle="modal"-->
                                            <a href="update?id=${u.getUserID()}&u=2"  class="edit" ><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
<!--                                            <a href="#" onclick="doDelete('${p.getProductId()}')" class="delete" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>-->
                                        </td>
                                    </tr>
                                </c:forEach>                          
                            </tbody>
                        </table>

                        <!--                        <div class="clearfix">
                                                    <ul class="pagination">
                        <c:forEach begin="1" end="${a.totalPage}" var="l">
                            <li class="page-item ${indexPage==l?"active":""}"><a href="pagingmanager?index=${l}" class="page-link">${l}</a></li>
                        </c:forEach>
                    </ul>
                </div>-->

                    </div>
                </div>        
            </div>
        </c:if>
        <c:if test="${requestScope.action==3}">
            <div class="container-xl">
                <div class="table-responsive">
                    <div class="table-wrapper">
                        <div class="table-title">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h2>Manager <b>Category</b></h2>

                                    <h4><a href="category" class="fa fa-arrow-circle-left">Back</a></h4>
                                </div>
                                <div class="col-sm-6">
                                    <a href="addCategory.jsp" class="btn btn-success" ><i class="material-icons">&#xE147;</i> <span>Add New Category</span></a>				
                                    <!--                                    data-toggle="modal"-->
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>                                                            
                                    <th>CategoryId</th>
                                    <th>Name</th>   
                                    <th>Supplier ID</th>  
                                    <th>Action</th> 
                                </tr>

                            </thead>
                            <tbody>
                                <c:forEach items="${requestScope.allcategory}" var="c">
                                    <tr>
                                        <td>${c.getCategoryID() }</td>
                                        <td>${c.getCateName() }</td> 

                                        <td>${c.getSupplier_id()}</td>
                                        <td>
                                            <!--                                        data-toggle="modal"-->
                                            <a href="update?id=${c.getCategoryID()}&u=3"  class="edit" ><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                                            <a href="#" onclick="doDeleteC('${c.getCategoryID()}')" class="delete" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
                                        </td>
                                    </tr>
                                </c:forEach>                          
                            </tbody>
                        </table>

                        <div class="clearfix">
                            <ul class="pagination">
                                <c:forEach begin="1" end="${a.totalPage}" var="l">
                                    <li class="page-item ${indexPage==l?"active":""}"><a href="pagingmanager?index=${l}" class="page-link">${l}</a></li>
                                    </c:forEach>
                            </ul>
                        </div>

                    </div>
                </div>        
            </div>
        </c:if>
        <c:if test="${requestScope.action==4}">
            <div class="container-xl">
                <div class="table-responsive">
                    <div class="table-wrapper">
                        <div class="table-title">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h2>Manager <b>Supplier</b></h2>

                                    <h4><a href="category" class="fa fa-arrow-circle-left">Back</a></h4>
                                </div>
                                <div class="col-sm-6">
                                    <a href="addSupplier.jsp" class="btn btn-success" ><i class="material-icons">&#xE147;</i> <span>Add New Supplier</span></a>				
                                    <!--                                    data-toggle="modal"-->
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>                                                            
                                    <th>SupplierID</th>
                                    <th>Name</th>   
                                    <th>Address</th>  
                                    <th>Phone</th> 
                                    <th>Action</th> 
                                </tr>

                            </thead>
                            <tbody>
                                <c:forEach items="${requestScope.allsupplier}" var="s">
                                    <tr>
                                        <td>${s.getId() }</td>
                                        <td>${s.getName() }</td>                                        
                                        <td>${s.getAddress()}</td>
                                        <td>${s.getPhone()}</td>
                                        <td>
                                            <!--                                        data-toggle="modal"-->
                                            <a href="update?id=${s.getId()}&u=4"  class="edit" ><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                                            <a href="#" onclick="doDeleteS('${s.getId()}')" class="delete" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
                                        </td>
                                    </tr>
                                </c:forEach>                          
                            </tbody>
                        </table>

                        <div class="clearfix">
                            <ul class="pagination">
                                <c:forEach begin="1" end="${a.totalPage}" var="l">
                                    <li class="page-item ${indexPage==l?"active":""}"><a href="pagingmanager?index=${l}" class="page-link">${l}</a></li>
                                    </c:forEach>
                            </ul>
                        </div>

                    </div>
                </div>        
            </div>
        </c:if>



    </body>
</html>