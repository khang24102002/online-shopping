/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import model.User;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Category;
import model.Product;

/**
 *
 * @author admin
 */
public class DAOAccount extends DBContext {

    public User check(String username, String password) {
        String sql = "SELECT [userId]\n"
                + "      ,[username]\n"
                + "      ,[gender]\n"
                + "      ,[phone]\n"
                + "      ,[email]\n"
                + "      ,[address]\n"
                + "      ,[password]\n"
                + "      ,[roleID]\n"
                + "      ,[create_date]\n"
                + "  FROM [dbo].[User]\n"
                + "  where [username]=? and password=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, username);
            st.setString(2, password);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                User u = new User(rs.getInt("userId"), username,
                        rs.getString("gender"), rs.getString("phone"),
                        rs.getString("email"), rs.getString("address"), password,
                        rs.getInt("roleID"), rs.getString("create_date"));
                return u;
            }
        } catch (SQLException e) {

        }
        return null;
    }

    public User check1(int userId, String password) {
        String sql = "SELECT [userId]\n"
                + "      ,[username]\n"
                + "      ,[gender]\n"
                + "      ,[phone]\n"
                + "      ,[email]\n"
                + "      ,[address]\n"
                + "      ,[password]\n"
                + "      ,[roleID]\n"
                + "      ,[create_date]\n"
                + "  FROM [dbo].[User]\n"
                + "  where [userId]=? and password=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            st.setString(2, password);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                User u = new User(userId, rs.getString("username"),
                        rs.getString("gender"), rs.getString("phone"),
                        rs.getString("email"), rs.getString("address"), password,
                        rs.getInt("roleID"), rs.getString("create_date"));
                return u;
            }
        } catch (SQLException e) {

        }
        return null;
    }

    //kiem tra xem tk vs id,username nay da ton tai chua
    public boolean existedUser(int userId) {
        String sql = "SELECT [userId]\n"
                + "      ,[username]\n"
                + "      ,[image]\n"
                + "      ,[gender]\n"
                + "      ,[phone]\n"
                + "      ,[email]\n"
                + "      ,[address]\n"
                + "      ,[password]\n"
                + "      ,[roleID]\n"
                + "      ,[create_date]\n"
                + "  FROM [dbo].[User]\n"
                + "  where [userId]=? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return true;
            }
        } catch (SQLException e) {

        }
        return false;
    }
//dang ky

    public void register(User a) {
        String sql = "INSERT INTO [dbo].[User]\n"
                + "           ([userId]\n"
                + "           ,[username]\n"
                + "           ,[gender]\n"
                + "           ,[phone]\n"
                + "           ,[email]\n"
                + "           ,[address]\n"
                + "           ,[password]\n"
                + "           ,[roleID]\n"
                + "           ,[create_date])\n"
                + "     VALUES\n"
                + "           (?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, a.getUserID());
            st.setString(2, a.getUsername());
            st.setString(3, a.getGender());
            st.setString(4, a.getPhone());
            st.setString(5, a.getEmail());
            st.setString(6, a.getAddress());
            st.setString(7, a.getPassword());
            st.setInt(8, a.getRoldID());
            st.setString(9, a.getCreate_date());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    //change pass
    public void change(User a) {
        String sql = "UPDATE [dbo].[User]\n"
                + "   SET \n"
                + "      [password] = ?    \n"
                + " WHERE userId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, a.getPassword());
            st.setInt(2, a.getUserID());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public List<User> getAllUser() {
        List<User> list = new ArrayList<>();
        String sql = "SELECT  * FROM [dbo].[User]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User u = new User(rs.getInt("userId"), rs.getString("username"),
                        rs.getString("gender"), rs.getString("phone"),
                        rs.getString("email"), rs.getString("address"), rs.getString("password"),
                        rs.getInt("roleID"), rs.getString("create_date"));
                list.add(u);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public User getUserById(int id) {

        String sql = "SELECT [userId]\n"
                + "      ,[username]\n"
                + "      ,[gender]\n"
                + "      ,[phone]\n"
                + "      ,[email]\n"
                + "      ,[address]\n"
                + "      ,[password]\n"
                + "      ,[roleID]\n"
                + "      ,[create_date]\n"
                + "  FROM [dbo].[User]\n"
                + "  where userId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                User u = new User(rs.getInt("userId"), rs.getString("username"),
                        rs.getString("gender"), rs.getString("phone"),
                        rs.getString("email"), rs.getString("address"), rs.getString("password"),
                        rs.getInt("roleID"), rs.getString("create_date"));
                return u;
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public void update(User a) {
        String sql = "UPDATE [dbo].[User]\n"
                + "   SET \n"
                + "      [username] = ?\n"
                + "      ,[gender] = ?\n"
                + "      ,[phone] = ?\n"
                + "      ,[email] = ?\n"
                + "      ,[address] = ?\n"
                + "      ,[password] = ?\n"
                + "      ,[roleID] = ?\n"
                + "      ,[create_date] = ?\n"
                + " WHERE userId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            st.setString(1, a.getUsername());
            st.setString(2, a.getGender());
            st.setString(3, a.getPhone());
            st.setString(4, a.getEmail());
            st.setString(5, a.getAddress());
            st.setString(6, a.getPassword());
            st.setInt(7, a.getRoldID());
            st.setString(8, a.getCreate_date());
            st.setInt(9, a.getUserID());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public static void main(String[] args) {
        DAOAccount c = new DAOAccount();
        User u=new User(10, "nam", "", "", "", "", "123", 1, "");
        c.update(u);
    }
}
