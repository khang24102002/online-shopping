/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Category;
import model.Order;
import model.OrderDate;
import model.OrderDetail;
import model.Product;
import model.ThongKe;

/**
 *
 * @author admin
 */
public class DAOSa extends DBContext {

    public List<Order> getAllOrder() {

        List<Order> list = new ArrayList<>();
        String sql = "SELECT [orderId]\n"
                + "      ,[name]\n"
                + "      ,[phone]\n"
                + "      ,[address]\n"
                + "      ,[order_date]\n"
                + "      ,[u_id]\n"
                + "      ,[totalmoney]\n"
                + "  FROM [dbo].[Order]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Order o = new Order(rs.getInt("orderId"),
                        rs.getString("name"),
                        rs.getString("phone"),
                        rs.getString("address"),
                        rs.getString("order_date"),
                        rs.getInt("u_id"),
                        rs.getInt("totalmoney")
                );
                list.add(o);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<OrderDetail> getAllOrderDetail() {

        List<OrderDetail> list = new ArrayList<>();
        String sql = "SELECT [orderDetail]\n"
                + "      ,[price]\n"
                + "      ,[quantity]\n"
                + "      ,[p_id]\n"
                + "  FROM [dbo].[OrderLine]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                OrderDetail od = new OrderDetail(rs.getInt("orderDetail"),
                        rs.getInt("price"),
                        rs.getInt("quantity"),
                        rs.getInt("p_id")
                );
                list.add(od);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<OrderDate> OrderAllowDate() {

        List<OrderDate> list = new ArrayList<>();
        String sql = "select order_date,count(o.orderid) as soluong,sum(totalmoney) as tong from [Order] as o group by order_date";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                OrderDate od = new OrderDate(rs.getString(1),
                        rs.getInt(2),
                        rs.getInt(3)
                );
                list.add(od);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Order> getByOrderDesc() {

        List<Order> list = new ArrayList<>();
        String sql = "SELECT [orderId]\n"
                + "      ,[name]\n"
                + "      ,[phone]\n"
                + "      ,[address]\n"
                + "      ,[order_date]\n"
                + "      ,[u_id]\n"
                + "      ,[totalmoney]\n"
                + "  FROM [dbo].[Order]\n"
                + "  order by totalmoney desc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Order o = new Order(rs.getInt("orderId"),
                        rs.getString("name"),
                        rs.getString("phone"),
                        rs.getString("address"),
                        rs.getString("order_date"),
                        rs.getInt("u_id"),
                        rs.getInt("totalmoney")
                );
                list.add(o);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Order> getByOrderAsc() {

        List<Order> list = new ArrayList<>();
        String sql = "SELECT [orderId]\n"
                + "      ,[name]\n"
                + "      ,[phone]\n"
                + "      ,[address]\n"
                + "      ,[order_date]\n"
                + "      ,[u_id]\n"
                + "      ,[totalmoney]\n"
                + "  FROM [dbo].[Order]\n"
                + "  order by totalmoney asc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Order o = new Order(rs.getInt("orderId"),
                        rs.getString("name"),
                        rs.getString("phone"),
                        rs.getString("address"),
                        rs.getString("order_date"),
                        rs.getInt("u_id"),
                        rs.getInt("totalmoney")
                );
                list.add(o);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Order> searchOrder(String key1, Integer price1, Integer price2) {
        List<Order> list = new ArrayList<>();
        String sql = "SELECT [orderId]\n"
                + "      ,[name]\n"
                + "      ,[phone]\n"
                + "      ,[address]\n"
                + "      ,[order_date]\n"
                + "      ,[u_id]\n"
                + "      ,[totalmoney]\n"
                + "  FROM [dbo].[Order]\n"
                + " where 1=1";
        if (key1 != null && !key1.equals("")) {
            sql += " and name like '%" + key1 + "%'";
        }
        if (price1 != null) {
            sql += " and totalmoney>=" + price1;
        }
        if (price2 != null) {
            sql += " and totalmoney<=" + price2;
        }

        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Order o = new Order();
                o.setOrderId(rs.getInt("orderId"));
                o.setName(rs.getString("name"));
                o.setPhone(rs.getString("phone"));
                o.setAddress(rs.getString("address"));
                o.setOrder_date(rs.getString("order_date"));
                o.setUid(rs.getInt("u_id"));
                o.setTotalmoney(rs.getInt("totalmoney"));

                list.add(o);
            }
        } catch (SQLException e) {
        }
        return list;
    }

    public List<OrderDetail> searchOrderDetail(Integer price1, Integer price2, Integer amount) {
        List<OrderDetail> list = new ArrayList<>();
        String sql = "SELECT [orderDetail]\n"
                + "      ,[price]\n"
                + "      ,[quantity]\n"
                + "      ,[p_id]\n"
                + "  FROM [dbo].[OrderLine]\n"
                + "  where 1=1";
        if (price1 != null) {
            sql += " and price>=" + price1;
        }
        if (price2 != null) {
            sql += " and price<=" + price2;
        }
        if (amount != null) {
            sql += " and quantity =" + amount;
        }

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                OrderDetail od = new OrderDetail();
                od.setOrderDetail(rs.getInt("orderDetail"));
                od.setPrice(rs.getInt("price"));
                od.setQuantity(rs.getInt("quantity"));
                od.setPid(rs.getInt("p_id"));
                list.add(od);
            }
        } catch (SQLException e) {
        }
        return list;
    }

    public List<ThongKe> ThongKe() {

        List<ThongKe> list = new ArrayList<>();
        String sql = "select p.productId,p.name,p.price,p.amount, od.tong  from Product as p left join (select  p_id,count(quantity)as tong from OrderLine  group by p_id) as od on p.productId=od.p_id";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                ThongKe t = new ThongKe(rs.getInt(1),
                        rs.getString(2),
                        rs.getInt(3),
                        rs.getInt(4),
                        rs.getInt(5)
                );
                list.add(t);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public static void main(String[] args) {
        DAOSa d = new DAOSa();
       List<Order> l=d.searchOrder("cus1", null, null);
        System.out.println(l.get(0).getOrder_date());
    }
}
