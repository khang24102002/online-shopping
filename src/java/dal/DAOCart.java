/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import model.User;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Product;

/**
 *
 * @author admin
 */
public class DAOCart extends DBContext {

    public User getAccount(String user, String pass) {
        String sql = "SELECT [userId]\n"
                + "      ,[username]\n"
                + "      ,[gender]\n"
                + "      ,[phone]\n"
                + "      ,[email]\n"
                + "      ,[address]\n"
                + "      ,[password]\n"
                + "      ,[roleID]\n"
                + "      ,[create_date]\n"
                + "  FROM [dbo].[User]\n"
                + "  where [username]=? and password=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, user);
            st.setString(2, pass);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                User u = new User(rs.getInt("userId"), user,
                        rs.getString("gender"), rs.getString("phone"),
                        rs.getString("email"), rs.getString("address"), pass,
                        rs.getInt("roleID"), rs.getString("create_date"));
                return u;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<Product> getAllProduct() {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT [productId]\n"
                + "      ,[name]\n"
                + "      ,[image]\n"
                + "      ,[price]\n"
                + "      ,[discount]\n"
                + "      ,[description]\n"
                + "      ,[amount]\n"
                + "      ,[sell_date]\n"
                + "      ,[category_id]\n"
                + "  FROM [dbo].[Product]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product p = new Product(
                        rs.getInt("productId"),
                        rs.getString("name"),
                        rs.getString("image"),
                        rs.getInt("price"),
                        rs.getString("color"),
                        rs.getInt("discount"),
                        rs.getString("description"),
                        rs.getInt("amount"),
                        rs.getString("sell_date"),
                        rs.getInt("category_id")
                );
                list.add(p);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public Product getProductById(int id) {

        String sql = "SELECT [productId]\n"
                + "      ,[name]\n"
                + "      ,[image]\n"
                + "      ,[price]\n"
                + "      ,[color]\n"
                + "      ,[discount]\n"
                + "      ,[description]\n"
                + "      ,[amount]\n"
                + "      ,[sell_date]\n"
                + "      ,[category_id]\n"
                + "  FROM [dbo].[Product]\n"
                + "where productId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Product p = new Product(
                        rs.getInt("productId"),
                        rs.getString("name"),
                        rs.getString("image"),
                        rs.getInt("price"),
                        rs.getString("color"),
                        rs.getInt("discount"),
                        rs.getString("description"),
                        rs.getInt("amount"),
                        rs.getString("sell_date"),
                        rs.getInt("category_id")
                );
                return p;
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

}
