/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import model.Cart;
import model.Category;
import model.FeedBack;
import model.Item;
import model.Product;
import model.Supplier;
import model.User;

/**
 *
 * @author User
 */
public class DAO extends DBContext {

    public List<Category> getAllCategory() {

        List<Category> list = new ArrayList<>();
        String sql = "SELECT [CategoryID]\n"
                + "      ,[CateName]\n"
                + "      ,[supplier_id]\n"
                + "  FROM [dbo].[Category]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category c = new Category(rs.getInt("CategoryID"), rs.getString("CateName"), rs.getInt("supplier_id"));
                list.add(c);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public void updateCategory(Category c) {
        String sql = "UPDATE [dbo].[Category]\n"
                + "   SET \n"
                + "      [CateName] = ?\n"
                + "      ,[supplier_id] = ?\n"
                + " WHERE CategoryID =?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            st.setString(1, c.getCateName());
            st.setInt(2, c.getSupplier_id());
            st.setInt(3, c.getCategoryID());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void insertCategory(Category c) {
        String sql = "INSERT INTO [dbo].[Category]\n"
                + "           ([CategoryID]\n"
                + "           ,[CateName]\n"
                + "           ,[supplier_id])\n"
                + "     VALUES\n"
                + "           (?,?,?)\n";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, c.getCategoryID());
            st.setString(2, c.getCateName());
            st.setInt(3, c.getSupplier_id());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    //tim 1 category khi co id
    public Category getCatergoryById(int id) {

        String sql = "select * from Category where CategoryID=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Category c = new Category(rs.getInt("CategoryID"), rs.getString("CateName"), rs.getInt("supplier_id"));
                return c;
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public Supplier getSupplierById(int id) {

        String sql = "SELECT [id]\n"
                + "      ,[name]\n"
                + "      ,[address]\n"
                + "      ,[phone]\n"
                + "  FROM [dbo].[Supplier]\n"
                + "  where id=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Supplier s = new Supplier(rs.getInt("id"), rs.getString("name"), rs.getString("address"), rs.getString("phone"));
                return s;
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

//get all product
    public List<Product> getAllProduct() {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT [productId]\n"
                + "      ,[name]\n"
                + "      ,[image]\n"
                + "      ,[price]\n"
                + "      ,[color]\n"
                + "      ,[discount]\n"
                + "      ,[description]\n"
                + "      ,[amount]\n"
                + "      ,[sell_date]\n"
                + "      ,[category_id]\n"
                + "  FROM [dbo].[Product]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product p = new Product(
                        rs.getInt("productId"),
                        rs.getString("name"),
                        rs.getString("image"),
                        rs.getInt("price"),
                        rs.getString("color"),
                        rs.getInt("discount"),
                        rs.getString("description"),
                        rs.getInt("amount"),
                        rs.getString("sell_date"),
                        rs.getInt("category_id")
                );
                list.add(p);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> getAllProductByAsc() {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT [productId]\n"
                + "      ,[name]\n"
                + "      ,[image]\n"
                + "      ,[price]\n"
                + "      ,[color]\n"
                + "      ,[discount]\n"
                + "      ,[description]\n"
                + "      ,[amount]\n"
                + "      ,[sell_date]\n"
                + "      ,[category_id]\n"
                + "  FROM [dbo].[Product]\n"
                + "  order by price";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product p = new Product(
                        rs.getInt("productId"),
                        rs.getString("name"),
                        rs.getString("image"),
                        rs.getInt("price"),
                        rs.getString("color"),
                        rs.getInt("discount"),
                        rs.getString("description"),
                        rs.getInt("amount"),
                        rs.getString("sell_date"),
                        rs.getInt("category_id")
                );
                list.add(p);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> getAllProductByDesc() {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT [productId]\n"
                + "      ,[name]\n"
                + "      ,[image]\n"
                + "      ,[price]\n"
                + "      ,[color]\n"
                + "      ,[discount]\n"
                + "      ,[description]\n"
                + "      ,[amount]\n"
                + "      ,[sell_date]\n"
                + "      ,[category_id]\n"
                + "  FROM [dbo].[Product]\n"
                + "  order by price desc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product p = new Product(
                        rs.getInt("productId"),
                        rs.getString("name"),
                        rs.getString("image"),
                        rs.getInt("price"),
                        rs.getString("color"),
                        rs.getInt("discount"),
                        rs.getString("description"),
                        rs.getInt("amount"),
                        rs.getString("sell_date"),
                        rs.getInt("category_id")
                );
                list.add(p);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    //tim prodcut theo cid;
    public List<Product> getProductsByCid(int cid) {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT [productId]\n"
                + "      ,[name]\n"
                + "      ,[image]\n"
                + "      ,[price]\n"
                + "      ,[color]\n"
                + "      ,[discount]\n"
                + "      ,[description]\n"
                + "      ,[amount]\n"
                + "      ,[sell_date]\n"
                + "      ,[category_id]\n"
                + "  FROM [dbo].[Product]\n"
                + "  where 1=1";
        if (cid != 0) {
            sql += " and category_id =" + cid;
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            //st.setInt(1, cid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product p = new Product();
                p.setProductId(rs.getInt("productId"));
                p.setName(rs.getString("name"));
                p.setImage(rs.getString("image"));
                p.setPrice(rs.getInt("price"));
                p.setColor(rs.getString("color"));
                p.setDiscount(rs.getInt("discount"));
                p.setDescription(rs.getString("description"));
                p.setAmount(rs.getInt("amount"));
                p.setSell_date(rs.getString("sell_date"));

                p.setCategory_id(rs.getInt("category_id"));
                list.add(p);
            }
        } catch (SQLException e) {

        }
        return list;
    }
    //tim san pham tuong tu

    public List<Product> getSampleProduct(int cid, int pid) {
        List<Product> list = new ArrayList<>();
        String sql = "select * from Product where category_id=? and productId!=?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, cid);
            st.setInt(2, pid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product p = new Product();
                p.setProductId(rs.getInt("productId"));
                p.setName(rs.getString("name"));
                p.setImage(rs.getString("image"));
                p.setPrice(rs.getInt("price"));
                p.setColor(rs.getString("color"));
                p.setDiscount(rs.getInt("discount"));
                p.setDescription(rs.getString("description"));
                p.setAmount(rs.getInt("amount"));
                p.setSell_date(rs.getString("sell_date"));
                p.setCategory_id(rs.getInt("category_id"));
                list.add(p);
            }
        } catch (SQLException e) {
        }
        return list;
    }

    public Product getProductById(int id) {

        String sql = "SELECT [productId]\n"
                + "      ,[name]\n"
                + "      ,[image]\n"
                + "      ,[price]\n"
                + "      ,[color]\n"
                + "      ,[discount]\n"
                + "      ,[description]\n"
                + "      ,[amount]\n"
                + "      ,[sell_date]\n"
                + "      ,[category_id]\n"
                + "  FROM [dbo].[Product]\n"
                + "where productId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Product p = new Product(
                        rs.getInt("productId"),
                        rs.getString("name"),
                        rs.getString("image"),
                        rs.getInt("price"),
                        rs.getString("color"),
                        rs.getInt("discount"),
                        rs.getString("description"),
                        rs.getInt("amount"),
                        rs.getString("sell_date"),
                        rs.getInt("category_id")
                );
                return p;
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public Product getProductByAmountMin() {
        String sql = "SELECT [productId]\n"
                + "      ,[name]\n"
                + "      ,[image]\n"
                + "      ,[price]\n"
                + "      ,[color]\n"
                + "      ,[discount]\n"
                + "      ,[description]\n"
                + "      ,[amount]\n"
                + "      ,[sell_date]\n"
                + "      ,[category_id]\n"
                + "  FROM [dbo].[Product]\n"
                + "  where amount=(select MIN(amount) from Product )";
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Product p = new Product(
                        rs.getInt("productId"),
                        rs.getString("name"),
                        rs.getString("image"),
                        rs.getInt("price"),
                        rs.getString("color"),
                        rs.getInt("discount"),
                        rs.getString("description"),
                        rs.getInt("amount"),
                        rs.getString("sell_date"),
                        rs.getInt("category_id")
                );
                return p;
            }
        } catch (SQLException e) {

        }
        return null;
    }

    public Product getNewProduct() {
        String sql = "SELECT top 1* from Product\n"
                + "order by productId desc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Product p = new Product(
                        rs.getInt("productId"),
                        rs.getString("name"),
                        rs.getString("image"),
                        rs.getInt("price"),
                        rs.getString("color"),
                        rs.getInt("discount"),
                        rs.getString("description"),
                        rs.getInt("amount"),
                        rs.getString("sell_date"),
                        rs.getInt("category_id")
                );
                return p;
            }
        } catch (SQLException e) {

        }
        return null;
    }

    //tim feedback khi co p_id
    public List<FeedBack> getFbById(int id) {
        List<FeedBack> list = new ArrayList<>();
        String sql = "SELECT [fbId]\n"
                + "      ,[fb_date]\n"
                + "      ,[content]\n"
                + "      ,[user_id]\n"
                + "      ,[p_id]\n"
                + "  FROM [dbo].[Feedback]\n"
                + "  where p_id=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                FeedBack f = new FeedBack();
                f.setFbID(rs.getInt("fbId"));
                f.setFb_date(rs.getString("fb_date"));
                f.setContent(rs.getString("content"));
                f.setUser_id(rs.getInt("user_id"));
                f.setP_id(rs.getInt("p_id"));
                list.add(f);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    //tim san pham theo dieu kien
    public List<Product> search(String key2, String key3, Integer price1, Integer price2, String date, int cid) {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT [productId]\n"
                + "      ,[name]\n"
                + "      ,[image]\n"
                + "      ,[price]\n"
                + "      ,[color]\n"
                + "      ,[discount]\n"
                + "      ,[description]\n"
                + "      ,[amount]\n"
                + "      ,[sell_date]\n"
                + "      ,[category_id]\n"
                + "  FROM [dbo].[Product]\n"
                + "  where 1=1";
        if (key2 != null && !key2.equals("")) {
            sql += " and name like '%" + key2 + "%'";
        }
        if (key3 != null && !key3.equals("")) {
            sql += " and color like '%" + key3 + "%'";
        }
        if (cid != 0) {
            sql += " and category_id=" + cid;
        }
        if (price1 != null) {
            sql += " and price>=" + price1;
        }
        if (price2 != null) {
            sql += " and price<=" + price2;
        }
        if (date != null) {
            sql += " and sell_date like'%" + date + "%'";
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            //st.setInt(1, cid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product p = new Product();
                p.setProductId(rs.getInt("productId"));
                p.setName(rs.getString("name"));
                p.setImage(rs.getString("image"));
                p.setPrice(rs.getInt("price"));
                p.setColor(rs.getString("color"));
                p.setDescription(rs.getString("description"));
                p.setAmount(rs.getInt("amount"));
                p.setSell_date(rs.getString("sell_date"));
                p.setCategory_id(rs.getInt("category_id"));
                list.add(p);
            }
        } catch (SQLException e) {
        }
        return list;
    }

    //dem so trang rong db
    public int getTotalPage() {
        String sql = "select count(*) from Product";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int total = rs.getInt(1);
                int countPage = 0;
                countPage = total / 6;
                if (total % 6 != 0) {
                    countPage++;
                }
                return countPage;
            }

        } catch (Exception e) {
        }
        return 0;
    }

    //phan trang
    public List<Product> getPaging(int index) {
        List<Product> list = new ArrayList<>();
        String sql = "select * from Product\n"
                + "order by productId\n"
                + "offset ? rows \n"
                + "FETCH FIRST 6 rows only";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, (index - 1) * 6);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product p = new Product();
                p.setProductId(rs.getInt("productId"));
                p.setName(rs.getString("name"));
                p.setImage(rs.getString("image"));
                p.setPrice(rs.getInt("price"));
                p.setColor(rs.getString("color"));
                p.setDiscount(rs.getInt("discount"));
                p.setDescription(rs.getString("description"));
                p.setAmount(rs.getInt("amount"));
                p.setSell_date(rs.getString("sell_date"));
                p.setCategory_id(rs.getInt("category_id"));
                list.add(p);
            }
        } catch (SQLException e) {

        }
        return list;
    }

    public void addOrder(User u, Cart cart) {
        LocalDate curDate = LocalDate.now();
        String date = curDate.toString();
        try {
            //add order
            String sql = "insert into [Order] values(?,?,?,?,?,?)";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, u.getUsername());
            st.setString(2, u.getPhone());
            st.setString(3, u.getAddress());
            st.setString(4, date);
            st.setInt(5, u.getUserID());
            st.setDouble(6, cart.getTotalMoney());
            st.executeUpdate();
            //lay id cua order vua add
            String sql1 = "select top 1 [orderId] from [Order] order by [orderId] desc";
            PreparedStatement st1 = connection.prepareStatement(sql1);
            ResultSet rs = st1.executeQuery();
            //add bang OrderDetail
            if (rs.next()) {
                int oid = rs.getInt("orderId");
                for (Item i : cart.getItems()) {
                    String sql2 = "insert into [OrderLine] values(?,?,?,?)";
                    PreparedStatement st2 = connection.prepareStatement(sql2);
                    st2.setInt(1, oid);
                    st2.setInt(2, i.getPrice());
                    st2.setInt(3, i.getQuantity());
                    st2.setInt(4, i.getProduct().getProductId());

                    st2.executeUpdate();
                }
            }
            //cap nhat lai so luong san pham
            String sql3 = "update Product set amount=amount-? where productId=?";
            PreparedStatement st3 = connection.prepareStatement(sql3);
            for (Item i : cart.getItems()) {
                st3.setInt(1, i.getQuantity());
                st3.setInt(2, i.getProduct().getProductId());
                st3.executeUpdate();
            }
        } catch (SQLException e) {

        }
    }

    public List<Supplier> getAllSupplier() {
        List<Supplier> list = new ArrayList<>();
        String sql = "SELECT [id]\n"
                + "      ,[name]\n"
                + "      ,[address]\n"
                + "      ,[phone]\n"
                + "  FROM [dbo].[Supplier]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Supplier s = new Supplier(
                        rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("address"),
                        rs.getString("phone")
                );
                list.add(s);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public void updateSupplier(Supplier s) {
        String sql = "UPDATE [dbo].[Supplier]\n"
                + "   SET \n"
                + "      [name] = ?\n"
                + "      ,[address] = ?\n"
                + "      ,[phone] = ?\n"
                + " WHERE id =?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            st.setString(1, s.getName());
            st.setString(2, s.getAddress());
            st.setString(3, s.getPhone());
            st.setInt(4, s.getId());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void insertSupplier(Supplier s) {
        String sql = "INSERT INTO [dbo].[Supplier]\n"
                + "           ([id]\n"
                + "           ,[name]\n"
                + "           ,[address]\n"
                + "           ,[phone])\n"
                + "     VALUES\n"
                + "           (?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, s.getId());
            st.setString(2, s.getName());
            st.setString(3, s.getAddress());
            st.setString(4, s.getPhone());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public int getFeedBackID() {
        String sql = "select max(fbId) from Feedback";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {

        }
        return 0;
    }

    public void insertFeedBack(FeedBack f) {
        String sql = "INSERT INTO [dbo].[Feedback]\n"
                + "           ([fbId]\n"
                + "           ,[fb_date]\n"
                + "           ,[content]\n"
                + "           ,[user_id]\n"
                + "           ,[p_id])\n"
                + "     VALUES\n"
                + "           (?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, f.getFbID());
            st.setString(2, f.getFb_date());
            st.setString(3, f.getContent());
            st.setInt(4, f.getUser_id());
            st.setInt(5, f.getP_id());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public static void main(String[] args) {
        DAO d = new DAO();
        List<Product> l = d.getSampleProduct(1, 1);
        System.out.println(l.size());
    }
}
