/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Category;
import model.Product;

/**
 *
 * @author admin
 */
public class DAOcrud extends DBContext {

    public List<Product> getAllProduct() {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT [productId]\n"
                + "      ,[name]\n"
                + "      ,[image]\n"
                + "      ,[price]\n"
                + "      ,[color]\n"
                + "      ,[discount]\n"
                + "      ,[description]\n"
                + "      ,[amount]\n"
                + "      ,[sell_date]\n"
                + "      ,[category_id]\n"
                + "  FROM [dbo].[Product]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product p = new Product(
                        rs.getInt("productId"),
                        rs.getString("name"),
                        rs.getString("image"),
                        rs.getInt("price"),
                        rs.getString("color"),
                        rs.getInt("discount"),
                        rs.getString("description"),
                        rs.getInt("amount"),
                        rs.getString("sell_date"),
                        rs.getInt("category_id")
                );
                list.add(p);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public void insert(Product p) {
        String sql = "INSERT INTO [dbo].[Product]\n"
                + "           ([productId]\n"
                + "           ,[name]\n"
                + "           ,[image]\n"
                + "           ,[price]\n"
                + "           ,[color]\n"
                + "           ,[discount]\n"
                + "           ,[description]\n"
                + "           ,[amount]\n"
                + "           ,[sell_date]\n"
                + "           ,[category_id])\n"
                + "     VALUES\n"
                + "           (?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, p.getProductId());
            st.setString(2, p.getName());
            st.setString(3, p.getImage());
            st.setInt(4, p.getPrice());
            st.setString(5, p.getColor());
            st.setInt(6, p.getDiscount());
            st.setString(7, p.getDescription());
            st.setInt(8, p.getAmount());
            st.setString(9, p.getSell_date());
            st.setInt(10, p.getCategory_id());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    //tim 1 category khi co id
    public Product getProductById(int id) {

        String sql = "SELECT [productId]\n"
                + "      ,[name]\n"
                + "      ,[image]\n"
                + "      ,[price]\n"
                + "      ,[color]\n"
                + "      ,[discount]\n"
                + "      ,[description]\n"
                + "      ,[amount]\n"
                + "      ,[sell_date]\n"
                + "      ,[category_id]\n"
                + "  FROM [dbo].[Product]\n"
                + "  where productId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Product p = new Product(
                        rs.getInt("productId"),
                        rs.getString("name"),
                        rs.getString("image"),
                        rs.getInt("price"),
                        rs.getString("color"),
                        rs.getInt("discount"),
                        rs.getString("description"),
                        rs.getInt("amount"),
                        rs.getString("sell_date"),
                        rs.getInt("category_id")
                );
                return p;
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    //delete a product
    public void delete(int id) {
        String sql = "DELETE FROM [dbo].[Product]\n"
                + "      WHERE productId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void deleteCategory(int id) {
        String sql = "DELETE FROM [dbo].[Category]\n"
                + "      WHERE CategoryID=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void deleteSupplier(int id) {
        String sql = "DELETE FROM [dbo].[Supplier]\n"
                + "      WHERE id=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    //tim 1 product khi co id

    public Category getCatergoryById(int id) {

        String sql = "SELECT [CategoryID]\n"
                + "      ,[CateName]\n"
                + "      ,[supplier_id]\n"
                + "  FROM [dbo].[Category]\n"
                + "  where CategoryID=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Category c = new Category(rs.getInt("CategoryID"), rs.getString("CateName"), rs.getInt("supplier_id"));
                return c;
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    //update
    public void update(Product p) {
        String sql = "UPDATE [dbo].[Product]\n"
                + "   SET \n"
                + "      [name] = ?\n"
                + "      ,[image] = ?\n"
                + "      ,[price] = ?\n"
                + "      ,[color] = ?\n"
                + "      ,[discount] = ?\n"
                + "      ,[description] = ?\n"
                + "      ,[amount] = ?\n"
                + "      ,[sell_date] = ?\n"
                + "      ,[category_id] = ?\n"
                + " WHERE productId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            st.setString(1, p.getName());
            st.setString(2, p.getImage());
            st.setInt(3, p.getPrice());
            st.setString(4, p.getColor());
            st.setInt(5, p.getDiscount());
            st.setString(6, p.getDescription());
            st.setInt(7, p.getAmount());
            st.setString(8, p.getSell_date());
            st.setInt(9, p.getCategory_id());
            st.setInt(10, p.getProductId());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

}
