/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author User
 */
public class FeedBack {
    private int fbID;
    private String  fb_date;
    private String content;
    private int  user_id;
    private int p_id;

    public FeedBack() {
    }

    public FeedBack(int fbID, String fb_date, String content, int user_id, int p_id) {
        this.fbID = fbID;
        this.fb_date = fb_date;
        this.content = content;
        this.user_id = user_id;
        this.p_id = p_id;
    }

    public int getFbID() {
        return fbID;
    }

    public void setFbID(int fbID) {
        this.fbID = fbID;
    } 

    public String getFb_date() {
        return fb_date;
    }

    public void setFb_date(String fb_date) {
        this.fb_date = fb_date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getP_id() {
        return p_id;
    }

    public void setP_id(int p_id) {
        this.p_id = p_id;
    }

   
   
    
    
    
}
