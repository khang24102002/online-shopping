/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author admin
 */
public class ThongKe {
    private int id;
    private String name;
    private int priceBD;
    private int amountCon;
    private int amountBan;

    public ThongKe() {
    }

    public ThongKe(int id, String name, int priceBD, int amountCon, int amountBan) {
        this.id = id;
        this.name = name;
        this.priceBD = priceBD;
        this.amountCon = amountCon;
        this.amountBan = amountBan;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPriceBD() {
        return priceBD;
    }

    public void setPriceBD(int priceBD) {
        this.priceBD = priceBD;
    }

    public int getAmountCon() {
        return amountCon;
    }

    public void setAmountCon(int amountCon) {
        this.amountCon = amountCon;
    }

    public int getAmountBan() {
        return amountBan;
    }

    public void setAmountBan(int amountBan) {
        this.amountBan = amountBan;
    }

    
    
}
