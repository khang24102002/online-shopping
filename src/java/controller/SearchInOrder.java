/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.DAO;
import dal.DAOSa;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Order;
import model.OrderDate;

/**
 *
 * @author admin
 */
@WebServlet(name = "SearchInOrder", urlPatterns = {"/searchorder"})
public class SearchInOrder extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SearchInOrder</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SearchInOrder at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String active_raw = request.getParameter("active");
        String key_raw = request.getParameter("key");      
        int active, key ;
        DAOSa a = new DAOSa();
        DAO d = new DAO();
        try {
            active = Integer.parseInt(active_raw);
            key = Integer.parseInt(key_raw);
            if (key == 1) {
                List<Order> list = a.getAllOrder();
                request.setAttribute("listorder", list);
                request.setAttribute("active", active);
                request.setAttribute("act", 1);
            }
            if (key == 2) {
                List<OrderDate> list1 = a.OrderAllowDate();
                request.setAttribute("listorder", list1);
                request.setAttribute("active", active);
                request.setAttribute("act", 2);
            }
            if (key == 3) {
                List<Order> list2 = a.getByOrderDesc();
                request.setAttribute("listorder", list2);
                request.setAttribute("active", active);
                request.setAttribute("act", 1);
            }
            if (key == 4) {
                List<Order> list3 = a.getByOrderAsc();
                request.setAttribute("listorder", list3);
                request.setAttribute("active", active);
                request.setAttribute("act", 1);
            }          
            request.getRequestDispatcher("statis.jsp").forward(request, response);
        } catch (NumberFormatException e) {
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
