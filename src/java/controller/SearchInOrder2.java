/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.DAO;
import dal.DAOSa;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Order;
import model.OrderDetail;
import model.Product;

/**
 *
 * @author admin
 */
@WebServlet(name = "SearchInOrder2", urlPatterns = {"/searchorders"})
public class SearchInOrder2 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SearchInOrder2</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SearchInOrder2 at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DAOSa a = new DAOSa();
        DAO d = new DAO();
//        String hide_raw = request.getParameter("hide");
        String active_raw = request.getParameter("active");
        String name = request.getParameter("data");
        String t_money = request.getParameter("tmoney");
        String f_money = request.getParameter("fmoney");
        int active;
        Integer tmoney, fmoney;
        try {
            active = Integer.parseInt(active_raw);
            tmoney = ((t_money == null) || (t_money.equals("")))
                    ? null : Integer.parseInt(t_money);
            fmoney = ((f_money == null) || (f_money.equals("")))
                    ? null : Integer.parseInt(f_money);
            List<Order> list4 = a.searchOrder(name, fmoney, tmoney);
             
             
            request.setAttribute("listorder", list4);
            request.setAttribute("active", active);
            request.setAttribute("act", 1);
        } catch (NumberFormatException e) {
        }
        request.getRequestDispatcher("statis.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DAOSa a = new DAOSa();
        DAO d = new DAO();
//        String hide_raw = request.getParameter("hide");
        String active_raw = request.getParameter("active");
        String t_money = request.getParameter("tmoney1");
        String f_money = request.getParameter("fmoney1");
         String amount_raw= request.getParameter("amount");
        int active;
        Integer tmoney, fmoney,amount;
        try {
            active = Integer.parseInt(active_raw);
            tmoney = ((t_money == null) || (t_money.equals("")))
                    ? null : Integer.parseInt(t_money);
            fmoney = ((f_money == null) || (f_money.equals("")))
                    ? null : Integer.parseInt(f_money);
            amount = ((amount_raw == null) || (amount_raw.equals("")))
                    ? null : Integer.parseInt(amount_raw);
            List<OrderDetail> list5 = a.searchOrderDetail(fmoney, tmoney,amount);
            List<Product> p=d.getAllProduct();
             request.setAttribute("listp", p);
            request.setAttribute("listod", list5);
            request.setAttribute("active", active);
            request.setAttribute("act", 2);
        } catch (NumberFormatException e) {
        }
        request.getRequestDispatcher("statis.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
