/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.DAO;
import dal.DAOCart;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Cart;
import model.Category;
import model.Item;
import model.Product;

/**
 *
 * @author User
 */
@WebServlet(name = "ProductServlet", urlPatterns = {"/product"})
public class ProductServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id_raw = request.getParameter("cid");
        List<Product> listProductByID;
        int id;
        DAO d = new DAO();
        try {
            id = Integer.parseInt(id_raw);
            if (id == 0) {
                listProductByID = d.getPaging(id + 1);
            } else {
                listProductByID = d.getProductsByCid(id);
            }

            List<Category> list = d.getAllCategory();
            request.setAttribute("category", list);

//            Cookie[] arr = request.getCookies();
//            String txt = "";
//            if (arr != null) {
//                for (Cookie o : arr) {
//                    if (o.getName().equals("cart")) {
//                        txt += o.getValue();
//                        o.setMaxAge(0);
//                        response.addCookie(o);
//                    }
//                }
//            }
//            Cart cart = new Cart(txt, list1);
//            List<Item> listItem = cart.getItems();
//            int n;
//            if (listItem != null) {
//                n = listItem.size();
//            } else {
//                n = 0;
//            }
//            request.setAttribute("size", n);
//            request.setAttribute("cart", cart);
            request.setAttribute("tag", id);
            request.setAttribute("products", listProductByID);
        } catch (NumberFormatException e) {
            System.out.println(e);
        }
        request.getRequestDispatcher("home.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
