/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.DAO;
import dal.DAOAccount;
import dal.DAOcrud;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Category;
import model.Product;
import model.Supplier;
import model.User;

/**
 *
 * @author admin
 */
@WebServlet(name = "UpdateProduct", urlPatterns = {"/update"})
public class UpdateProduct extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UpdateProduct</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UpdateProduct at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id_raw = request.getParameter("id");
        String u_raw = request.getParameter("u");
        int id, u;
        try {
            id = Integer.parseInt(id_raw);
            u = Integer.parseInt(u_raw);

            DAOcrud cdb = new DAOcrud();
            DAO d = new DAO();
            DAOAccount a = new DAOAccount();
            Product p = cdb.getProductById(id);
            User user = a.getUserById(id);
            Supplier s = d.getSupplierById(id);
            Category c = d.getCatergoryById(id);
            if (u == 1) {
                request.setAttribute("product", p);
            }
            if (u == 2) {
                request.setAttribute("user", user);
            }
            if (u == 3) {
                request.setAttribute("category", c);
            }
            if (u == 4) {
                request.setAttribute("supplier", s);
            }
            request.setAttribute("u", u);
            request.getRequestDispatcher("updateProduct.jsp").forward(request, response);
        } catch (NumberFormatException e) {
            System.out.println(e);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        DAOcrud cdb = new DAOcrud();
        DAOAccount a = new DAOAccount();
        DAO d=new DAO();
        String hide_raw = request.getParameter("hide");
        int hide = Integer.parseInt(hide_raw);
        if (hide == 1) {
            String pid_raw = request.getParameter("pid");
            String p_name = request.getParameter("pname");
            String image = request.getParameter("image");
            String p_price = request.getParameter("price");
            String p_amount = request.getParameter("amount");
            String date = request.getParameter("date");
            int pid, price, amount;
            try {
                pid = Integer.parseInt(pid_raw);
                price = Integer.parseInt(p_price);
                amount = Integer.parseInt(p_amount);
                Product pNew = new Product(pid, p_name, image, price,"", 10, "", amount, date, 1);
                cdb.update(pNew);
                response.sendRedirect("list");
            } catch (NumberFormatException e) {
            }
        }
        if (hide == 2) {
            String role_id = request.getParameter("role");
            String u_id = request.getParameter("uid");
            String u_name = request.getParameter("uname");
            String gender = request.getParameter("gender");
            String u_phone = request.getParameter("phone");
            String email = request.getParameter("email");
            String u_address = request.getParameter("address");
            String pass = request.getParameter("password");
            int role, uid;
            try {
                role = Integer.parseInt(role_id);
                uid = Integer.parseInt(u_id);
                User uNew = new User(uid, u_name, gender, u_phone, email, u_address, pass, role, "");
                a.update(uNew);
                response.sendRedirect("list");
            } catch (NumberFormatException e) {
            }
        }
        if (hide == 3) {
            String cid_id = request.getParameter("cid");
            String c_name= request.getParameter("cname");
            String supplier_id = request.getParameter("supplierid");           
            int cid, supplierid;
            try {
                cid = Integer.parseInt(cid_id);
                supplierid = Integer.parseInt(supplier_id);
                Category cNew = new Category(cid, c_name, supplierid);
                d.updateCategory(cNew);
                response.sendRedirect("list");
            } catch (NumberFormatException e) {
            }
        }
        if (hide == 4) {
            String s_id = request.getParameter("sid");
            String s_name = request.getParameter("sname");
            String s_address = request.getParameter("saddress");
            String sphone = request.getParameter("sphone");
            
            int sid;
            try {
                sid = Integer.parseInt(s_id);
                Supplier sNew= new Supplier(sid, s_name, s_address, sphone);              
                d.updateSupplier(sNew);
                response.sendRedirect("list");
            } catch (NumberFormatException e) {
            }
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
