/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Category;
import model.Product;

/**
 *
 * @author admin
 */
@WebServlet(name = "SearhServlet", urlPatterns = {"/search"})
public class SearhServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DAO d = new DAO();
        String color = request.getParameter("color");
        String name = request.getParameter("name");
        String to_price = request.getParameter("toprice");
        String from_price = request.getParameter("fromprice");
        String date = request.getParameter("date");
        String cid_raw = request.getParameter("key");
        Integer toprice, fromprice, cid;
        List<Category> list = d.getAllCategory();
//        Product p = d.getProductByAmountMin();
//        Product newP=d.getNewProduct();
        request.setAttribute("category", list);
//        request.setAttribute("bestseller", p);
//        request.setAttribute("newproduct", newP);
        String hide_raw = request.getParameter("hide");
        int hide=Integer.parseInt(hide_raw);
        if (hide ==0 ) {
            List<Product> list0 = d.getAllProduct();
            request.setAttribute("products", list0);
        }
        if (hide == 1) {
            List<Product> list1 = d.getAllProductByAsc();
            request.setAttribute("products", list1);
        }
        if (hide == 2) {
            List<Product> list2 = d.getAllProductByDesc();
            request.setAttribute("products", list2);
        }else{
        try {
            cid = (cid_raw == null) ? 0 : Integer.parseInt(cid_raw);
             
                fromprice = ((from_price == null) || (from_price.equals("")))
                        ? null : Integer.parseInt(from_price);
                toprice = ((to_price == null) || (to_price.equals("")))
                        ? null : Integer.parseInt(to_price);
                List<Product> products = d.search(name, color, fromprice, toprice, date, cid);

                request.setAttribute("products", products);
            
        } catch (NumberFormatException e) {

        }
        }
        request.getRequestDispatcher("home.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
