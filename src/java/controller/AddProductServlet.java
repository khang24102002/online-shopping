/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.DAO;
import dal.DAOcrud;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Category;
import model.Product;
import model.Supplier;

/**
 *
 * @author User
 */
@WebServlet(name = "AddProductServlet", urlPatterns = {"/add"})
public class AddProductServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        //lay du lieu tu form
        String add_raw = request.getParameter("add");
        int add = Integer.parseInt(add_raw);
        DAOcrud cdb = new DAOcrud();
        DAO d=new DAO();
        if (add == 1) {
            String id_raw = request.getParameter("id");
            String name = request.getParameter("name");
            String price_raw = request.getParameter("price");
            String image = request.getParameter("image");
            String sell_date = request.getParameter("date");
            String amount_raw = request.getParameter("amount");
            int id, price, amount;

            try {
                id = Integer.parseInt(id_raw);
                price = Integer.parseInt(price_raw);
                amount = Integer.parseInt(amount_raw);
                Product c = cdb.getProductById(id);
                if (c == null) {
                    Product pNew = new Product(id, name, image, price,"", 5, "", amount, sell_date, 1);
                    cdb.insert(pNew);
                    response.sendRedirect("list");
                } else {
                    request.setAttribute("ms", "Id :" + id + " have existed!!!");
                    request.getRequestDispatcher("addnewProduct.jsp").forward(request, response);
                }
            } catch (NumberFormatException e) {
                System.out.println(e);
            }
        }
        if (add == 3) {
            String cid_raw = request.getParameter("cid");
            String cname = request.getParameter("cname");
            String supplierid_raw = request.getParameter("supplierid");
            
            int cid,supplierid;
           
            try {
                cid = Integer.parseInt(cid_raw);              
                supplierid = Integer.parseInt(supplierid_raw);
                Category c = d.getCatergoryById(cid);
                if (c == null) {
                    Category cNew = new Category(cid, cname, supplierid);
                    d.insertCategory(cNew);
                    response.sendRedirect("list");
                } else {
                    request.setAttribute("ms", "Id :" + cid + " have existed!!!");
                    request.getRequestDispatcher("addCategory.jsp").forward(request, response);
                }
            } catch (NumberFormatException e) {
                System.out.println(e);
            }
        }
        if (add == 4) {
            String sid_raw = request.getParameter("sid");
            String sname = request.getParameter("sname");
            String s_address = request.getParameter("saddress");
            String sphone = request.getParameter("sphone");
            
            int sid;
           
            try {
                sid = Integer.parseInt(sid_raw);
                
                Supplier s= d.getSupplierById(sid);
                if (s == null) {
                    Supplier sNew = new Supplier(sid, sname, s_address, sphone);
                    d.insertSupplier(sNew);
                    response.sendRedirect("list");
                } else {
                    request.setAttribute("ms", "Id :" + sid + " have existed!!!");
                    request.getRequestDispatcher("addSupplier.jsp").forward(request, response);
                }
            } catch (NumberFormatException e) {
                System.out.println(e);
            }
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
